<?php
/**
 * Created by PhpStorm.
 * User: honza
 * Date: 26. 5. 2018
 * Time: 18:22
 */

namespace App\Model\Auth;

use App\Model\EmployeeModel;
use Nette;
use Nette\Security;

class Authenticator implements Security\IAuthenticator
{
    use Nette\SmartObject;

    /** @var EmployeeModel */
    private $employeeModel;


    public function __construct(EmployeeModel $employeeModel)
    {
        $this->employeeModel = $employeeModel;
    }

    public function authenticate(array $credentials)
    {
        $login = $credentials[0];
        $password = $credentials[1];
        $row = $this->employeeModel->getEmployeeByLogin($login)->fetch();

        if (!$row || !Security\Passwords::verify($password, $row->vysetrujici_password)) {
            throw new Security\AuthenticationException('Chybně zadaný uživatel či heslo.', self::INVALID_CREDENTIAL);
        }

        $arr = $row->toArray();
        unset($arr['vysetrujici_password']);
        unset($arr['vysetrujici_role']);
        return new Security\Identity($row->vysetrujici_id, $row->ref('role')->role_nazev, $arr);
    }

}