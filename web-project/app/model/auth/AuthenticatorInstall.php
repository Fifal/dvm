<?php
/**
 * Created by PhpStorm.
 * User: Fifal
 * Date: 03.07.2018
 * Time: 16:33
 *
 * Class creates user with role Install when in database has no user
 */

namespace App\Model\Auth;

use Nette;
use Nette\Security;
use Nette\Security\AuthenticationException;
use Nette\Security\IIdentity;

class AuthenticatorInstall implements Security\IAuthenticator
{

    /**
     * Performs an authentication against e.g. database.
     * and returns IIdentity on success or throws AuthenticationException
     * @return IIdentity
     * @throws AuthenticationException
     */
    function authenticate(array $credentials)
    {
        return new Security\Identity(0, 'install');
    }
}