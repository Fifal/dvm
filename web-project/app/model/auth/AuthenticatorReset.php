<?php
/**
 * Created by PhpStorm.
 * User: Fifal
 * Date: 03.07.2018
 * Time: 16:33
 *
 * Class creates user with role Install when in database has no user
 */

namespace App\Model\Auth;

use App\Model\AdminResetModel;
use Nette;
use Nette\Security;
use Nette\Security\AuthenticationException;
use Nette\Security\IIdentity;

class AuthenticatorReset implements Security\IAuthenticator
{
    /** @var AdminResetModel*/
    public $adminResetModel;

    public function __construct(AdminResetModel $model)
    {
        $this->adminResetModel = $model;
    }

    /**
     * Performs an authentication against e.g. database.
     * and returns IIdentity on success or throws AuthenticationException
     * @return IIdentity
     * @throws AuthenticationException
     */
    function authenticate(array $credentials)
    {
        $token = '';
        if(isset($credentials[0])) {
            $token = $credentials[0];
        }

        if($token == '') {
            throw new Security\AuthenticationException('Token neexistuje!', self::INVALID_CREDENTIAL);
        }

        $tokenUsed = false;

        $tokenRow = $this->adminResetModel->getToken($token)->fetch();

        if($tokenRow->admin_reset_pouzito){
            $tokenUsed = true;
        }

        if(!$tokenUsed) {
            return new Security\Identity(0, 'reset', ['token' => $token]);
        }else{
            throw new Security\AuthenticationException('Token již byl použit!', self::INVALID_CREDENTIAL);
        }
    }
}