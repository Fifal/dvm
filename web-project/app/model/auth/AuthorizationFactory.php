<?php
/**
 * Created by PhpStorm.
 * User: honza
 * Date: 26. 5. 2018
 * Time: 23:12
 */

namespace App\Model\Auth;

use Nette;

class AuthorizationFactory
{
    /**
     * @return Nette\Security\Permission
     */
    public static function create()
    {
        $acl = new Nette\Security\Permission;

        $acl->addRole('guest');
        $acl->addRole('user');
        $acl->addRole('admin', 'user');
        $acl->addRole('install');
        $acl->addRole('reset');

        // User profile edit
        $acl->addResource('edit-profile');
        $acl->allow('user', 'edit-profile');

        $acl->addResource('vysetreni');
        $acl->allow('user', 'vysetreni');

        // Administration - db-upload, db-import...
        $acl->addResource('admin');
        $acl->allow('admin', 'admin');

        // Installation
        $acl->addResource('installation');
        $acl->allow('install', 'installation');

        // Generate password
        $acl->addResource('generate-admin-password');
        $acl->allow('reset', 'generate-admin-password');

        $acl->deny('user', ['admin']);
        $acl->deny('guest', ['vysetreni', 'generate-admin-password']);
        $acl->deny('install', ['vysetreni', 'admin', 'edit-profile', 'generate-admin-password']);


        return $acl;
    }
}