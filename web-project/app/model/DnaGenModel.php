<?php
/**
 * Created by PhpStorm.
 * User: HARD
 * Date: 12.07.2018
 * Time: 13:27
 */

namespace App\Model;

use Nette;

/**
 * Class DnaGenModel
 *
 * Table name: dna_gen
 * Table structure:
 *      dna_gen_id => PK, int
 *      dna_gen_typ => varchar
 *
 * @package App\Model
 */
class DnaGenModel
{
    use Nette\SmartObject;

    const TABLE_NAME = 'dna_gen';

    const COL_GEN_ID = 'dna_gen_id';
    const COL_GEN_TYPE = 'dna_gen_typ';

    /** @var Nette\Database\Context */
    public $database;

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    /**
     * Returns dna_gen by dna_gen_id
     *
     * @param $id : dna_gen_id
     * @return Nette\Database\Table\Selection
     */
    public function getDnaGenById($id)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_GEN_ID, $id);
    }

    /**
     * Returns dna_gen list
     *
     * @return Nette\Database\Table\Selection
     */
    public function getDnaGenList()
    {
        return $this->database->table(self::TABLE_NAME);
    }

    /**
     * Inserts into dna_gen
     *
     * @param $data
     * @return bool|int|Nette\Database\Table\ActiveRow
     */
    public function insertDna($data)
    {
        return $this->database->table(self::TABLE_NAME)
            ->insert($data);
    }

    /**
     * Deletes from dna_gen by dna_gen_id
     *
     * @param $id : dna_gen_id
     * @return int
     */
    public function deleteDnaById($id)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_GEN_ID, $id)
            ->delete();
    }

    /**
     * Returns variant with Genotyp LIKE
     *
     * @param $value
     * @return Nette\Database\Table\Selection
     */
    public function getDnaGenLikeGenotyp($value)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_GEN_TYPE . ' LIKE', '%' . $value . '%');
    }

}