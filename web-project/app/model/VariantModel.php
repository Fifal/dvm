<?php
/**
 * Created by PhpStorm.
 * User: Fifal
 * Date: 03.06.2018
 * Time: 23:22
 */

namespace App\Model;

use Nette;

/**
 * Class VariantModel
 *
 * Table name: varianta
 * Table structure:
 *      varianta_id => PK, int
 *      varianta_gen_id => FK, int
 *      varianta_hgvs => text
 *      varianta_rs => int
 *      varianta_clinvar_datum => datetime (YYYY-MM-DD HH:MI:SS)
 *      varianta_clinvar_protein => text
 *      varianta_clinvar_vyznam => text
 *      varianta_poznamka => text
 *
 * @package App\Model
 */
class VariantModel
{
    use Nette\SmartObject;

    /** @var \Nette\Database\Context*/
    public $database;
    /** @var Nette\DI\Container */
    public $container;
    /** @var PanelGeneModel */
    public $panelGeneModel;

    const TABLE_NAME = 'varianta';

    const COL_ID = 'varianta_id';
    const COL_GEN_ID = 'varianta_gen_id';
    const COL_HGVS = 'varianta_hgvs';
    const COL_RS = 'varianta_rs';
    const COL_CLINVAR_DATE = 'varianta_clinvar_datum';
    const COL_CLINVAR_PROTEIN = 'varianta_clinvar_protein';
    const COL_CLINVAR_VERDICT = 'varianta_clinvar_vyznam';
    const COL_NOTE = 'varianta_poznamka';
    const COL_DATE_FROM = 'varianta_datum_od';
    const COL_DATE_TO = 'varianta_datum_do';
    const COL_VERDICT = 'varianta_vyznam';

    public function __construct(Nette\Database\Context $database, Nette\DI\Container $container, PanelGeneModel $panelGeneModel)
    {
        $this->database = $database;
        $this->container = $container;
        $this->panelGeneModel = $panelGeneModel;
    }

    /**
     * Returns Variant by ID
     *
     * @param $id : varianta_id
     * @return Nette\Database\Table\Selection
     */
    public function getVariantById($id)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_ID, $id);
    }

    /**
     * Returns Variant by RS
     *
     * @param $rs : varianta_rs
     * @return Nette\Database\Table\Selection
     */
    public function getVariantByRs($rs)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_RS, $rs);
    }

    /**
     * Returns Variant by Gene ID
     *
     * @param $genId : varianta_gen_id
     * @return Nette\Database\Table\Selection
     */
    public function getVariantByGeneId($genId)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_GEN_ID, $genId);
    }

    /**
     * Updates Variant row
     *
     * @param $id : varianta_id
     * @param $data : the structure of the table is in the class description
     * @return int
     */
    public function updateVariantById($id, $data)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_ID, $id)
            ->update($data);
    }

    /**
     * Inserts into Variant
     *
     * @param $data : the structure of the table is in the class description
     * @return bool|int|Nette\Database\Table\ActiveRow
     */
    public function insertVariant($data)
    {
        return $this->database->table(self::TABLE_NAME)
            ->insert($data);
    }

    /**
     * Deletes from variant (varianta) by id (varianta_id)
     *
     * @param $id : varianta_id
     * @return int
     */
    public function deleteVariantById($id)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_ID, $id)
            ->delete();
    }

    /**
     * Returns variants older than (actual date - date in config.local.neon)
     *      - if not set default value is 2 years
     * @return Nette\Database\Table\Selection
     */
    public function getOldVariants(){
        $date = new Nette\Utils\DateTime();

        // Loads interval from config.local.neon if is set
        $params =  $this->container->getParameters();
        if (isset($params['variants']) && isset($params['variants']['interval']))
        {
            $interval = $params['variants']['interval'];
            $date->modify('-' . $interval);
        } else
        {
            $date->modify('-2 years');
        }

        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_DATE_FROM . '<=', $date->format('Y-m-d'));
    }

    /**
     * Returns list of all variants
     *
     * @param null $panelId
     * @return array|Nette\Database\Table\Selection
     */
    public function listVariants($panelId = null)
    {
        if($panelId == null)
        {
            return $this->database->table(self::TABLE_NAME);
        }else
        {
            $panelGenes = $this->panelGeneModel->getGenesIdsInPanel($panelId);
            if(!empty($panelGenes))
            {
                return $this->database->table(self::TABLE_NAME)->where(self::COL_GEN_ID . ' IN(' . implode(',', $panelGenes) . ')');
            }else
            {
                return [];
            }
        }
    }

    /**
     * Returns variant with RS LIKE
     *
     * @param $value
     * @return Nette\Database\Table\Selection
     */
    public function getVariantLikeRS($value)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_RS . ' LIKE', '%' . $value . '%');
    }

    /**
     * Returns variant with clin verdict LIKE
     *
     * @param $value
     * @return Nette\Database\Table\Selection
     */
    public function getVariantLikeClinVerdict($value)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_CLINVAR_VERDICT . ' LIKE', '%' . $value . '%');
    }

    /**
     * Returns variant with HGVS LIKE
     *
     * @param $value
     * @return Nette\Database\Table\Selection
     */
    public function getVariantLikeHGVS($value)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_HGVS . ' LIKE', '%' . $value . '%');
    }

}