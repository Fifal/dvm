<?php
/**
 * Created by PhpStorm.
 * User: HARD
 * Date: 07.06.2018
 * Time: 17:37
 */

namespace App\Model;

use Nette;

/**
 * Class ExaminationModel
 *
 * Table name: vysetreni
 * Table structure:
 *      vysetreni_id => PK, int
 *      vysetreni_vysetrujici_id => FK, int
 *      vysetreni_dna_id => FK, int
 *      vysetreni_panel_id = FK, int
 *
 * @package App\Model
 */
class ExaminationModel
{
    use Nette\SmartObject;

    const TABLE_NAME = 'vysetreni';

    const COL_ID = 'vysetreni_id';
    const COL_EMPLOYEE_ID = 'vysetreni_vysetrujici_id';
    const COL_DNA_ID = 'vysetreni_dna_id';
    const COL_PANEL_ID = 'vysetreni_panel_id';
    const COL_TIMESTAMP = 'vysetreni_datum';

    /** @var Nette\Database\Context */
    public $database;

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    /**
     * Returns examination (vysetreni) by id (vysetreni_id)
     *
     * @param $id : vysetreni_id
     * @return Nette\Database\Table\Selection
     */
    public function getExaminationById($id)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_ID, $id);
    }

    /**
     * Returns examinations by employee id (vysetreni_vysetrujici_id)
     *
     * @param $emp_id : vysetreni_vysetrujici_id
     * @return Nette\Database\Table\Selection
     */
    public function getExaminationByEmpID($emp_id)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_EMPLOYEE_ID, $emp_id);
    }

    /**
     * Return examination (vysetreni) by dna id (vysetreni_dna_id)
     *
     * @param $dna_id : vysetreni_dna_id
     * @return Nette\Database\Table\Selection
     */
    public function getExaminationByDnaID($dna_id)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_DNA_ID, $dna_id);
    }

    /**
     * Returns examination (vysetreni) by parameters (dna_id, employee_id, panel_id)
     *
     * @param $dna_id
     * @param $emp_id
     * @param $panel_id
     * @return Nette\Database\Table\Selection
     */
    public function getExaminationByParameters($dna_id, $emp_id, $panel_id)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_DNA_ID, $dna_id)
            ->where(self::COL_EMPLOYEE_ID, $emp_id)
            ->where(self::COL_PANEL_ID, $panel_id);
    }

    /**
     * Inserts into examination (vysetreni)
     *
     * @param $data : the structure of the table is in the class description
     * @return bool|int|Nette\Database\Table\ActiveRow
     */
    public function insertExamination($data)
    {
        return $this->database->table(self::TABLE_NAME)
            ->insert($data);
    }

    /**
     * Updates examination (vysetreni) by id (vysetreni_id)
     *
     * @param $id : vysetreni_id
     * @param $data : the structure of the table is in the class description
     * @return int
     */
    public function updateExaminationById($id, $data)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_ID, $id)
            ->update($data);
    }

    /**
     * Deletes from examination (vysetreni) by id (vysetreni_id)
     *
     * @param $id : vysetreni_id
     * @return int
     */
    public function deleteExaminationById($id)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_ID, $id)
            ->delete();
    }

    /**
     * Returns list of all examinations
     *
     * @return Nette\Database\Table\Selection
     */
    public function listExaminations()
    {
        return $this->database->table(self::TABLE_NAME);
    }
}