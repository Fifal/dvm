<?php

namespace App\Model\Cron;

use App\Model\VariantModel;
use App\Utils\Varsome\VarsomeAPI;
use App\Utils\Varsome\VarsomeVariant;
use App\Utils\Varsome\VarsomeVariantEnum;
use Exception;
use Nette\DI\Container;
use Nette\Utils\DateTime;
use Symfony\Component\Console\Command\Command;
use \Symfony\Component\Console\Input\InputInterface;
use \Symfony\Component\Console\Output\OutputInterface;

/**
 * Command pro aktualizaci variant z ClinVar
 *      - Pouští z příkazové řádky web-project/www/ : "php index.php Cron:updateVariants"
 *
 * Class UpdateVariantsCommand
 * @package App\Model\Cron
 */
class UpdateVariantsCommand extends Command
{
    /** @var VariantModel @inject */
    public $variantModel;

    /** @var Container @inject */
    public $container;

    /** @var VarsomeAPI */
    private $varsomeApi;

    /** @var string Command name */
    const NAME = "Cron:updateVariants";

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Sets command name and description
     */
    protected function configure()
    {
        $this->setName(self::NAME)
            ->setDescription('Aktualizuje varianty pokud: aktualni datum > (datum pridani + config)');
    }

    /**
     * Executes command
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try
        {
            $params = $this->container->getParameters();
            $this->varsomeApi = new VarsomeAPI($params['varsome']['apikey']);
            $this->varsomeApi->addSourceDatabase(VarsomeVariantEnum::NCBI_CLINVAR2);
        } catch (Exception $ex)
        {
            \Tracy\Debugger::log('Nepodarilo se ziskat api klic pro VarSome', 'cron.error');
            throw $ex;
        }

        try
        {
            $variants = $this->variantModel->getOldVariants();
            $output->writeln('Pocet zaznamu k aktualizaci: ' . count($variants));

            $updated = 0;
            foreach ($variants as $variant)
            {
                if ($this->updateVariant($variant))
                {
                    $updated++;
                }
            }

            $output->writeln('Uspech! Pocet aktualizovanych variant: ' . $updated);
            \Tracy\Debugger::log(self::NAME . ' - uspech! Pocet aktualizovanych variant: ' . $updated, 'cron.success');
        } catch (Exception $ex)
        {
            $output->writeln('Nepodarilo se aktualizovat varianty: ' . $ex->getMessage());
            \Tracy\Debugger::log(self::NAME . ' - neuspech: ' . $ex->getMessage(), 'cron.error');
        }
    }

    /**
     * Updates variant from Varsome API
     * @param $variant : Selection
     * @return bool
     */
    private function updateVariant($variant)
    {
        // Gets variant RS
        $variantRs = $variant[VariantModel::COL_RS];

        // Find variants by RS
        $clinvarResults = $this->varsomeApi->getVariantLookup('rs' . $variantRs);

        /** @var $result VarsomeVariant */
        foreach ($clinvarResults as $result)
        {
            $lastDate = $result->getLastEvalDate();

            // If last eval date is null -> skip
            if ($lastDate == null)
            {
                continue;
            }

            $lastDate = new DateTime($lastDate);

            // If last eval date is newer than in database -> insert new variant
            if ($lastDate->format('Y-m-d') > $variantRs[VariantModel::COL_CLINVAR_DATE])
            {
                $data = [
                    VariantModel::COL_GEN_ID => $variant[VariantModel::COL_GEN_ID],
                    VariantModel::COL_HGVS => $variant[VariantModel::COL_HGVS],
                    VariantModel::COL_RS => $variant[VariantModel::COL_RS],
                    VariantModel::COL_CLINVAR_DATE => $lastDate->format('Y-m-d'),
                    VariantModel::COL_CLINVAR_PROTEIN => $variant[VariantModel::COL_CLINVAR_PROTEIN],
                    VariantModel::COL_CLINVAR_VERDICT => $result->getClinvarVerdict(),
                    VariantModel::COL_NOTE => $variant[VariantModel::COL_NOTE],
                ];

                // Insert new variant
                $this->variantModel->insertVariant($data);

                // Update old variant date_to
                $dateNow = new DateTime();
                $this->variantModel->updateVariantById($variant[VariantModel::COL_ID],
                    [
                        VariantModel::COL_DATE_TO => $dateNow->format('Y-m-d')
                    ]);
                return true;
            }
        }

        return false;
    }
}