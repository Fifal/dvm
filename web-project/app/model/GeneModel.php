<?php
/**
 * Created by PhpStorm.
 * User: HARD
 * Date: 07.06.2018
 * Time: 16:52
 */

namespace App\Model;

use Nette;

/**
 * Class GeneModel
 *
 * Table name: gen
 * Table structure:
 *      gen_id => PK, int
 *      gen_nazev => text
 *      gen_popis => text
 *      gen_synonyma = > text
 *      gen_reference => varchar
 *      gen_datum_od => date
 *      gen_datum_do => date
 *
 * @package App\Model
 */
class GeneModel
{
    use Nette\SmartObject;

    const TABLE_NAME = 'gen';

    const COL_ID = 'gen_id';
    const COL_NAME = 'gen_nazev';
    const COL_DESCRIPTION = 'gen_popis';
    const COL_SYNONYMS = 'gen_synonyma';
    const COL_REFERENCE = 'gen_reference';
    const COL_DATE_FROM = 'gen_datum_od';
    const COL_DATE_TO = 'gen_datum_do';

    /** @var Nette\Database\Context */
    public $database;

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    /**
     * Returns gene (gen) by id (gen_id)
     *
     * @param $id : gen_id
     * @return Nette\Database\Table\Selection
     */
    public function getGeneById($id)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_ID, $id);
    }

    /**
     * Returns gene (gen) by name (gen_nazev)
     *
     * @param $name : gen_nazev
     * @return Nette\Database\Table\Selection
     */
    public function getGeneByName($name)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_NAME, $name);
    }

    /**
     * Updates gene (gen) by id (gen_id)
     *
     * @param $id : gen_id
     * @param $data : the structure of the table is in the class description
     * @return int
     */
    public function updateGeneById($id, $data)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_ID, $id)
            ->update($data);
    }

    /**
     * Inserts into gene (gen)
     *
     * @param $data : the structure of the table is in the class description
     * @return bool|int|Nette\Database\Table\ActiveRow
     */
    public function insertGene($data)
    {
        return $this->database->table(self::TABLE_NAME)
            ->insert($data);
    }

    /**
     * Deletes from gene (gen) by id (gen_id)
     *
     * @param $id : gen_id
     * @return int
     */
    public function deleteGeneById($id)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_ID, $id)
            ->delete();
    }

    /**
     * Returns number of genes
     *
     * @return int
     */
    public function getNumberOfGenes()
    {
        return $this->database->table(self::TABLE_NAME)
            ->count("*");
    }


    /**
     * Returns all genes sorted by symbol (gene name) in ascending order
     *
     * @param null $limit
     * @param null $offset
     * @return Nette\Database\Table\Selection
     */
    public function getGenesOrderByNameASC($limit = null, $offset = null)
    {
        if ($limit) {
            return $this->database->table(self::TABLE_NAME)
                ->order(self::COL_NAME . ' ASC')
                ->limit($limit, $offset);
        } else {
            return $this->database->table(self::TABLE_NAME)
                ->order(self::COL_NAME . ' ASC');
        }
    }

    /**
     * Returns genes by first letter of last name (pacient_prijmeni)
     *
     * @param $firstLetter , $limit, $offset
     * @return Nette\Database\Table\Selection
     */
    public function getGenesByFirstLetter($firstLetter = '*', $limit = null, $offset = null)
    {
        if ($limit) {
            return $this->database->table(self::TABLE_NAME)
                ->where(self::COL_NAME . ' LIKE ?', $firstLetter . '%')
                ->order(self::COL_NAME . ' ASC')
                ->limit($limit, $offset);
        } else {
            return $this->database->table(self::TABLE_NAME)
                ->where(self::COL_NAME . ' LIKE ?', $firstLetter . '%')
                ->order(self::COL_NAME . ' ASC');
        }
    }

    /**
     * Returns gene by name and null date_to
     *
     * @param $name
     * @return Nette\Database\Table\Selection
     */
    public function getGeneByNameAndNullDateTo($name){
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_NAME, $name)
            ->where(self::COL_DATE_TO, null);
    }

    /**
     * Returns gene by name and reference
     *
     * @param $name
     * @param $reference
     * @return Nette\Database\Table\Selection
     */
    public function getGeneByNameAndRefSequence($name, $reference){
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_NAME, $name)
            ->where(self::COL_REFERENCE, $reference);
    }

    /**
     * Returns all genes
     *
     * @return Nette\Database\Table\Selection
     */
    public function listGenes(){
        return $this->database->table(self::TABLE_NAME);
    }

    /**
     * Returns genes with name LIKE
     *
     * @param $name
     * @return Nette\Database\Table\Selection
     */
    public function getGeneLikeName($name){
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_NAME . ' LIKE', '%' . $name . '%');
    }
}