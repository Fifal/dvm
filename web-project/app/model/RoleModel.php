<?php
/**
 * Created by PhpStorm.
 * User: fifal
 * Date: 07.06.2018
 * Time: 16:31
 */

namespace App\Model;

use Nette;

/**
 * Class RoleModel
 *
 * Table name: role
 * Table structure:
 *      role_id => PK, int
 *      role_name => varchar(50)
 *
 * @package App\Model
 */
class RoleModel
{
    use Nette\SmartObject;

    const TABLE_NAME = 'role';

    const COL_ID = 'role_id';
    const COL_NAME = 'role_nazev';

    /** @var Nette\Database\Context */
    public $database;

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    /**
     * Returns all roles from table
     *
     * @return Nette\Database\Table\Selection
     */
    public function getRoles()
    {
        return $this->database->table(self::TABLE_NAME);
    }

    /**
     * Returns role (role) by id (role_id)
     *
     * @param $id : role id
     * @return Nette\Database\Table\Selection
     */
    public function getRoleById($id)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_ID, $id);
    }

    /**
     * Returns role name (role) by name (role_nazev)
     *
     * @param $name : role name
     * @return Nette\Database\Table\Selection
     */
    public function getRoleByName($name)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_NAME, $name);
    }
}