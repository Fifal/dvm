<?php
/**
 * Created by PhpStorm.
 * User: Fifal
 * Date: 03.07.2018
 * Time: 16:09
 */

namespace App\Model;

use Nette;

class AdminResetModel
{
    use Nette\SmartObject;

    const TABLE_NAME = 'admin_reset';
    const COL_PASSWORD = 'admin_reset_heslo';
    const COL_USED = 'admin_reset_pouzito';

    /** @var Nette\Database\Context */
    public $database;

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    /**
     * Gets token by name
     *
     * @param $token
     * @return Nette\Database\Table\Selection
     */
    public function getToken($token)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_PASSWORD, $token);
    }

    /**
     * Inserts array of tokens into database
     *
     * @param $data
     * @return bool|int|Nette\Database\Table\ActiveRow
     */
    public function addTokens($data)
    {
        return $this->database->table(self::TABLE_NAME)
            ->insert($data);
    }

    /**
     * Deletes all tokens from database
     *
     * @return int
     */
    public function deleteAllTokens()
    {
        return $this->database->table(self::TABLE_NAME)
            ->delete();
    }

    /**
     * Updates database column admin_reset_pouzito to 1
     *
     * @param $token
     * @return int
     */
    public function setTokenUsed($token)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_PASSWORD, $token)
            ->update([self::COL_USED => 1]);
    }
}