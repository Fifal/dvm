<?php
/**
 * Created by PhpStorm.
 * User: HARD
 * Date: 07.06.2018
 * Time: 17:16
 */

namespace App\Model;

use Nette;

/**
 * Class PanelGeneModel
 *
 * Table name: panel_gen
 * Table structure:
 *      panel_gen_panel_id: PK, FK, int
 *      panel_gen_gen_id: PK, FK, int
 *
 * @package App\Model
 */
class PanelGeneModel
{
    use Nette\SmartObject;

    const TABLE_NAME = 'panel_gen';

    const COL_PANEL_ID = 'panel_gen_panel_id';
    const COL_GEN_ID = 'panel_gen_gen_id';

    /** @var Nette\Database\Context */
    public $database;

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    /**
     * Returns panel gen by panel id (panel_gen_panel_id)
     *
     * @param $panel_id : panel_gen_panel_id
     * @return Nette\Database\Table\Selection
     */
    public function getPanelGeneByPanelId($panel_id)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_PANEL_ID, $panel_id);
    }

    /**
     * Inserts into panel_gen
     *
     * @param $data : the structure of the table is in the class description
     * @return bool|int|Nette\Database\Table\ActiveRow
     */
    public function insertPanelGene($data)
    {
        return $this->database->table(self::TABLE_NAME)
            ->insert($data);
    }

    /**
     * Deletes from panel_gen by panel_gen_panel_id and panel_gen_gen_id
     *
     * @param $panel_id : panel_gen_panel_id
     * @param $gen_id : panel_gen_gen_id
     * @return int
     */
    public function deletePanelGeneByPK($panel_id, $gen_id)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where([
                self::COL_PANEL_ID => $panel_id,
                self::COL_GEN_ID => $gen_id
            ])
            ->delete();
    }

    /**
     * Deletes from panel_gen by panel_gen_panel_id
     *
     * @param $panel_id : panel_gen_panel_id
     * @return int
     */
    public function deletePanelGeneByPanelId($panel_id)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_PANEL_ID, $panel_id)
            ->delete();
    }

    /**
     * Returns genes in panel with given ID
     *
     * @param $panelId
     * @return array
     */
    public function getGenesIdsInPanel($panelId)
    {
        $selection = $this->database->table(self::TABLE_NAME)->where(self::COL_PANEL_ID, $panelId);

        $ids = array();
        foreach ($selection as $gene)
        {
            $ids[] = $gene[self::COL_GEN_ID];
        }
        return $ids;
    }

    /**
     * Returns panelgene by gene id
     *
     * @param $geneId
     * @return Nette\Database\Table\Selection
     */
    public function getPanelGeneByGeneId($geneId)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_GEN_ID, $geneId);
    }

}