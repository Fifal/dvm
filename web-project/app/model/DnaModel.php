<?php
/**
 * Created by PhpStorm.
 * User: Fifal
 * Date: 04.06.2018
 * Time: 0:06
 */

namespace App\Model;

use Nette;

/**
 * Class DnaModel
 *
 * Table name: dna
 * Table structure:
 *      dna_id => PK, int
 *      dna_typ_vzorku_id => FK, int
 *      dna_pacient_id => FK, int
 *      dna_cislo_vzorku => text
 *      dna_datum => datetime (YYYY-MM-DD HH:MI:SS)
 *      dna_kauzalita => text
 *
 * @package App\Model
 */
class DnaModel
{
    use Nette\SmartObject;

    const TABLE_NAME = 'dna';

    const COL_ID = 'dna_id';
    const COL_SAMPLE_TYPE_ID = 'dna_typ_vzorku_id';
    const COL_PATIENT_ID = 'dna_pacient_id';
    const COL_SAMPLE_NUMBER = 'dna_cislo_vzorku';
    const COL_DATE = 'dna_datum';
    const COL_CAUSALITY = 'dna_kauzalita';

    /** @var Nette\Database\Context */
    public $database;

    public function __construct(Nette\Database\Context $database)
    {
        $this->database = $database;
    }

    /**
     * Returns DNA by ID
     *
     * @param $id : dna_id
     * @return Nette\Database\Table\Selection
     */
    public function getDnaById($id)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_ID, $id);
    }

    /**
     * Returns DNA by Patient ID
     *
     * @param $patientId : dna_patient_id
     * @return Nette\Database\Table\Selection
     */
    public function getDnaByPatientId($patientId)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_PATIENT_ID, $patientId);
    }

    /**
     * Returns DNA by Sample Type ID
     *
     * @param $sampleId : dna_typ_vzorku_id
     * @return Nette\Database\Table\Selection
     */
    public function getDnaBySampleTypeId($sampleId)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_SAMPLE_TYPE_ID, $sampleId);
    }

    /**
     * Returns DNA by sample number
     *
     * @param $sampleNumber : dna_cislo_vzorku
     * @return false|Nette\Database\Table\ActiveRow
     */
    public function getDnaBySampleNumber($sampleNumber)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_SAMPLE_NUMBER, $sampleNumber)->fetch();
    }

    /**
     * Updates row by DNA ID
     *
     * @param $id : dna_id
     * @param $data : the structure of the table is in the class description
     * @return int
     */
    public function updateDnaById($id, $data)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_ID, $id)
            ->update($data);
    }

    /**
     * Inserts into dna
     *
     * @param $data : the structure of the table is in the class description
     * @return bool|int|Nette\Database\Table\ActiveRow
     */
    public function insertDna($data)
    {
        return $this->database->table(self::TABLE_NAME)
            ->insert($data);
    }

    /**
     * Deletes from dna by id (dna_id)
     *
     * @param $dna_id
     * @return int
     */
    public function deleteDnaById($dna_id)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_ID, $dna_id)
            ->delete();
    }

    /**
     * Returns list of all DNA samples
     *
     * @return Nette\Database\Table\Selection
     */
    public function listDna()
    {
        return $this->database->table(self::TABLE_NAME);
    }

    /**
     * Returns DNA with name LIKE
     *
     * @param $name
     * @return Nette\Database\Table\Selection
     */
    public function getDnaLikeSampleNumber($name)
    {
        return $this->database->table(self::TABLE_NAME)
            ->where(self::COL_SAMPLE_NUMBER . ' LIKE', '%' . $name . '%');
    }
}