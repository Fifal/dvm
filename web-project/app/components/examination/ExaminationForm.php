<?php
/**
 * Created by PhpStorm.
 * User: Fifal
 * Date: 27.08.2018
 * Time: 20:20
 */

namespace App\Components;

use App\Model\DnaModel;
use App\Model\ExaminationModel;
use App\Model\PanelModel;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class ExaminationForm extends Control
{
    /** @var ExaminationModel @inject */
    public $examinationModel;

    /** @var DnaModel @inject */
    public $dnaModel;

    /** @var PanelModel @inject */
    public $panelModel;

    public function __construct(ExaminationModel $examinationModel, DnaModel $dnaModel, PanelModel $panelModel)
    {
        parent::__construct();
        $this->examinationModel = $examinationModel;
        $this->dnaModel = $dnaModel;
        $this->panelModel = $panelModel;
    }

    public function create()
    {
        $form = $this->createEdit();

        $dnaSamples = $this->dnaModel->listDna()->fetchPairs(DnaModel::COL_ID, DnaModel::COL_SAMPLE_NUMBER);
        $form->addSelect(ExaminationModel::COL_DNA_ID, 'Vzorek DNA', $dnaSamples)
            ->setPrompt('Vyberte vzorek')
            ->addRule(Form::FILLED, 'Je nutné vybrat vzorek vyšetření!');

        return $form;
    }

    public function createEdit()
    {
        $form = new BootstrapForm;

        $panels = $this->panelModel->getListOfPanels()->fetchPairs(PanelModel::COL_ID, PanelModel::COL_NAME);
        foreach ($panels as $id => $panel)
        {
            $panelNote = $this->panelModel->database->table(PanelModel::TABLE_NAME)->where([PanelModel::COL_ID => $id])->fetch();
            if ($panelNote)
            {
                $panels[$id] = $panel . ' – ' . $panelNote[PanelModel::COL_NOTE];
            }
        }
        $form->addSelect(ExaminationModel::COL_PANEL_ID, 'Panel', $panels)
            ->setPrompt('Vyberte panel vyšetření')
            ->addRule(Form::FILLED, 'Je nutné vybrat panel vyšetření!');

        $form->addHidden(ExaminationModel::COL_EMPLOYEE_ID);

        return $form;
    }
}