<?php
/**
 * Created by PhpStorm.
 * User: Fifal
 * Date: 07.08.2018
 * Time: 18:21
 */

namespace App\Components;

use App\Model\GeneModel;
use App\Model\VariantModel;
use Nette\Forms\Form;
use function Sodium\add;

class VariantAddForm extends \Nette\Application\UI\Control
{
    /** @var GeneModel */
    private $geneModel;

    public function __construct(GeneModel $geneModel)
    {
        parent::__construct();
        $this->geneModel = $geneModel;
    }

    public function create()
    {
        $form = new BootstrapForm;

        $genes = $this->geneModel->listGenes()->fetchPairs(GeneModel::COL_ID, GeneModel::COL_NAME);

        $form->addSelect(VariantModel::COL_GEN_ID, 'Gen', $genes)
            ->setPrompt('Vyberte gen')
            ->addRule(Form::FILLED, 'Musíte přiřadit variantu ke genu');

        $form->addText(VariantModel::COL_RS, 'RS');
        $form->addText(VariantModel::COL_HGVS, 'HGVS');
        $form->addText(VariantModel::COL_VERDICT, 'Význam');
        $form->addText(VariantModel::COL_CLINVAR_VERDICT, 'Clinvar význam')
            ->setDisabled();
        $form->addText(VariantModel::COL_CLINVAR_DATE, 'Clinvar datum')
            ->setDisabled();
        $form->addText(VariantModel::COL_NOTE, 'Poznámka');

        return $form;
    }
}