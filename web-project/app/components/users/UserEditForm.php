<?php
/**
 * Created by PhpStorm.
 * User: Fifal
 * Date: 20.06.2018
 * Time: 20:19
 */
namespace App\Components;

use App\Model\EmployeeModel;
use App\Model\RoleModel;

class UserEditForm extends \Nette\Application\UI\Control
{
    /** @var EmployeeModel */
    private $employeeModel;

    /** @var RoleModel  */
    private $roleModel;

    public function __construct(EmployeeModel $employeeModel, RoleModel $roleModel)
    {
        parent::__construct();
        $this->employeeModel = $employeeModel;
        $this->roleModel = $roleModel;
    }

    /**
     * Creates new form for user edit
     *
     * @return \Nette\Application\UI\Form
     */
    public function create(){
        $form = new BootstrapForm;

        $form->addText('vysetrujici_login', 'Uživatelské jméno')
            ->setHtmlAttribute('class', 'form-control');

        // Select with all possible user roles from database table 'role'
        $roles = $this->roleModel->getRoles()->fetchAll();
        $options = array();
        foreach ($roles as $key => $role){
            $options[$key] = $role->role_nazev;
        }

        $form->addSelect('vysetrujici_role', 'Role', $options);

        $form->addText('vysetrujici_jmeno', 'Jméno');

        $form->addText('vysetrujici_prijmeni', 'Příjmení');

        $form->addText('vysetrujici_titul_pred', 'Titul před jménem');

        $form->addText('vysetrujici_titul_za', 'Titul za jménem');

        return $form;
    }
}