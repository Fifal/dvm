<?php
/**
 * Created by PhpStorm.
 * User: Fifal
 * Date: 04.07.2018
 * Time: 21:24
 */

namespace App\Components;

class BootstrapForm extends \Nette\Application\UI\Form
{
    /**
     * Sets Bootstrap forms style
     *
     * BootstrapForm constructor.
     * @param null $name
     */
    public function __construct($name = null)
    {
        parent::__construct($name);

        $renderer = $this->getRenderer();
        $renderer->wrappers['controls']['container'] = null;
        $renderer->wrappers['pair']['container'] = 'div class=form-group';
        $renderer->wrappers['pair']['.error'] = 'has-error';
        $renderer->wrappers['control']['container'] = 'div class=col-sm-12';
        $renderer->wrappers['label']['container'] = 'div class="col-sm-6 control-label"';
        $renderer->wrappers['control']['description'] = 'span class=help-block';
        $renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';
        $this->getElementPrototype()->class('form-horizontal');
    }

    public function addText($name, $label = null, $cols = null, $maxLength = null)
    {
        return parent::addText($name, $label, $cols, $maxLength)->setHtmlAttribute('class', 'form-control');
    }

    public function addSelect($name, $label = null, array $items = null, $size = null)
    {
        return parent::addSelect($name, $label, $items, $size)->setHtmlAttribute('class', 'form-control');
    }

    public function addPassword($name, $label = null, $cols = null, $maxLength = null)
    {
        return parent::addPassword($name, $label, $cols, $maxLength)->setHtmlAttribute('class', 'form-control');
    }

    public function addEmail($name, $label = null)
    {
        return parent::addEmail($name, $label)->setHtmlAttribute('class', 'form-control');
    }

    public function addTextArea($name, $label = null, $cols = null, $rows = null)
    {
        return parent::addTextArea($name, $label, $cols, $rows)->setHtmlAttribute('class', 'form-control');
    }

    public function addUpload($name, $label = null, $multiple = false)
    {
        return parent::addUpload($name, $label, $multiple)->setHtmlAttribute('class', 'form-control');
    }
}