<?php
/**
 * Created by PhpStorm.
 * User: honza
 * Date: 3. 7. 2018
 * Time: 17:32
 */

namespace App\Components;

use App\Model\DnaGenModel;
use App\Model\SampleTypeModel;
use Nette\Application\UI\Form;

class DnaEditForm extends \Nette\Application\UI\Control
{

    /**
     * @var SampleTypeModel
     */
    private $sampleModel;

    /**
     * @var DnaGenModel
     */
    private $dnaGenModel;

    /**
     * DnaEditForm constructor.
     * @param $sampleModel
     */
    public function __construct(SampleTypeModel $sampleModel, DnaGenModel $dnaGenModel)
    {
        parent::__construct();
        $this->sampleModel = $sampleModel;
        $this->dnaGenModel = $dnaGenModel;
    }

    /**
     * Creates new form for adding dna sample
     *
     * @return \Nette\Application\UI\Form
     */
    public function create(){
        $form = new BootstrapForm;

        $form = $this->createControls($form);

        return $form;
    }

    /**
     * @param $form
     * @return \Nette\Application\UI\Form
     */
    public function createControls($form){
        $form->addText('dna_cislo_vzorku', 'Číslo vzorku DNA')
            ->addRule(Form::FILLED, 'Musíte zadat číslo vzorku DNA');

        $samples = $this->sampleModel->getSamples();
        $options = array();
        foreach ($samples as $key => $sample){
            $options[$key] = $sample->typ_vzorku_nazev;
        }

        $form->addSelect('dna_typ_vzorku_id', 'Typ vzorku', $options)
            ->setPrompt('Vyberte typ vzorku')
            ->addRule(Form::FILLED, 'Musíte vybrat typ vzorku');

        $dnaGenlist = $this->dnaGenModel->getDnaGenList();
        $options = array();
        foreach ($dnaGenlist as $key => $dnaGen){
            $options[$key] = $dnaGen->dna_gen_typ;
        }

        $form->addText('dna_datum', 'Datum')
            ->setHtmlAttribute('class', 'form-control text-control')
            ->setHtmlAttribute('placeholder', 'dd.mm.YYYY')
            ->addRule(Form::PATTERN, 'Zadejte datum ve formátu dd.mm.YYYY', '^\s*(3[01]|[12][0-9]|0?[1-9])\.(1[012]|0?[1-9])\.((?:19|20)\d{2})\s*$')
            ->setRequired('Pole datum je povinné!');

        $form->addText('dna_kauzalita', 'Kauzalita');

        return $form;
    }
}