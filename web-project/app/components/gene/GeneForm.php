<?php
/**
 * Created by PhpStorm.
 * User: HARD
 * Date: 03.07.2018
 * Time: 17:15
 */

namespace App\Components;

use Nette\Application\UI\Form;

class GeneForm extends \Nette\Application\UI\Control
{

    /**
     * Creates a form for editing and adding a gene
     *
     * @return \Nette\Application\UI\Form
     */
    public function create(){

        $form = new BootstrapForm;

        $form->addText('gen_nazev', 'Název')
            ->addRule(Form::FILLED, 'Musíte zadat název genu.');

        $form->addText('gen_reference', 'Referenční sekvence');

        $form->addText('gen_popis', 'Popis');
        $form->addText('gen_synonyma', 'Synonyma');
        return $form;

    }

}