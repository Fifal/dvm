<?php
/**
 * Created by PhpStorm.
 * User: honza
 * Date: 26. 5. 2018
 * Time: 17:00
 */

namespace App\Presenters;

use App\Components\BootstrapForm;
use App\Components\UserEditForm;
use App\Components\UserPasswordForm;
use App\Model\EmployeeModel;
use App\Model\RoleModel;
use App\Utils\PasswordValidator;
use Nette;
use Nette\Application\UI;

class UserPresenter extends Nette\Application\UI\Presenter
{
    /** @var EmployeeModel */
    private $employeeModel;

    /** @var UserEditForm @inject */
    private $userEditFormFactory;

    /** @var UserPasswordForm @inject */
    private $userEditPasswordFormFactory;

    /** @var RoleModel @inject */
    public $roleModel;

    public function startup()
    {
        parent::startup();
        if (!$this->user->isInRole('admin') && !$this->user->isInRole('user') && $this->getAction() != "login") {
            throw new \Nette\Application\ForbiddenRequestException();
        }
    }

    public function __construct(EmployeeModel $employeeModel)
    {
        $this->employeeModel = $employeeModel;
    }

    /**
     * Injects user edit form factory
     *
     * @param UserEditForm $editForm
     */
    public function injectUserEditForm(UserEditForm $editForm)
    {
        $this->userEditFormFactory = $editForm;
    }

    /**
     * Injects user edit password form factory
     *
     * @param UserPasswordForm $passwordForm
     */
    public function injectUserEditPasswordForm(UserPasswordForm $passwordForm)
    {
        $this->userEditPasswordFormFactory = $passwordForm;
    }

    //
    //
    // LOGIN
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Creates new login form
     *
     * @return UI\Form
     */
    //TODO: možná přesunout do app/components/users/ a vytvořit form, ale kdoví... prio low
    protected function createComponentLoginForm()
    {
        $form = new BootstrapForm();

        $form->addText('login', 'Login:')
            ->setRequired()
            ->setHtmlAttribute('class', 'form-control mb-2');

        $form->addPassword('password', 'Heslo:')
            ->setRequired()
            ->setHtmlAttribute('class', 'form-control mb-2');

        $form->addSubmit('send', 'Přihlásit se')
            ->setHtmlAttribute('class', 'form-control btn btn-success');

        $form->onSuccess[] = [$this, 'loginFormSucceeded'];
        return $form;
    }

    /**
     * Handles login form
     *
     * @param UI\Form $form
     * @param Nette\Utils\ArrayHash $values
     * @throws Nette\Application\AbortException
     */
    public function loginFormSucceeded(UI\Form $form, Nette\Utils\ArrayHash $values)
    {
        try {
            $this->getUser()->login($values->login, $values->password);
        } catch (Nette\Security\AuthenticationException $e) {
            $this->flashMessage($e->getMessage(), 'error');
            return;
        }

        $this->flashMessage('Přihlášení proběhlo v pořádku', 'success');

        $roles = $this->roleModel->getRoleByName('admin')->fetch();

        if(isset($roles) && isset($roles->role_id) && $this->getUser()->isInRole('admin')){
            $this->redirect('Admin:');
        }
        $this->redirect('Homepage:');
    }

    /**
     * Logs out user
     *
     * @throws Nette\Application\AbortException
     */
    public function actionLogout()
    {
        $this->getUser()->logout();
        $this->flashMessage('Uživatel byl odhlášen.', 'success');
        $this->redirect('Homepage:');
    }

    //
    //
    // PROFILE EDIT
    //
    ////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * Sets user edit profile alert above the form
     *
     * @param $message
     * @param $type
     */
    public function setUserEditAlert($message, $type){
        $this->template->profileInfo = $message;
        $this->template->profileAction = $type;
    }

    /**
     * Sets user edit profile password alert above the form
     *
     * @param $message
     * @param $type
     */
    public function setUserEditPasswordAlert($message, $type){
        $this->template->passwordInfo = $message;
        $this->template->passwordAction = $type;
    }

    /**
     * Renders profile edit page
     *
     * @param $id
     * @throws Nette\Application\AbortException
     * @throws Nette\Application\ForbiddenRequestException
     */
    public function renderEditProfile($id)
    {

        // User allowed here?
        if (!$this->user->isAllowed('edit-profile')) {
            throw new Nette\Application\ForbiddenRequestException();
        }

        // If ID is not set -> bad url
        if (!isset($id)) {
            $this->redirect('Homepage:');
        }

        // User can edit only his details
        $userId = $this->getUser()->getIdentity()->getData()['vysetrujici_id'];
        if ($userId != $id) {
            throw new Nette\Application\ForbiddenRequestException();
        }


    }

    /**
     * Creates edit profile form
     *
     * @return UI\Form
     */
    protected function createComponentEditProfileForm()
    {
        $form = $this->userEditFormFactory->create();

        // Disable change of login and role
        $form['vysetrujici_login']->setDisabled(true);
        $form['vysetrujici_role']->setDisabled(true);

        $form->addHidden('vysetrujici_id', $this->getParameter('id'));
        $form->addSubmit('send', 'Upravit údaje')
            ->setHtmlAttribute('class', 'form-control btn btn-success');

        $form->onSuccess[] = [$this, 'editProfileSuccess'];

        $data = $this->employeeModel->getEmployeeById($this->getParameter('id'))->fetch();
        $form->setDefaults($data);

        return $form;
    }

    /**
     * Edits user profile details
     *
     * @param $form
     * @param $values
     */
    public function editProfileSuccess($form, $values)
    {
        $employeeId = $values->vysetrujici_id;

        // Nette Forms should handle this and should never be true
        if (isset($values->vysetrujici_login) || isset($values->vysetrujici_role)) {
            $this->setUserEditAlert('Není možné měnit roli nebo login uživatele!', 'danger');
            return;
        }

        unset($values->vysetrujici_id);
        $result = $this->employeeModel->updateEmployeeById($employeeId, $values);

        if ($result == 0) {
            $this->setUserEditAlert('Nastala chyba při odeslaní formuláře. Zkuste prosím formulář odeslat znovu.', 'danger');
            return;
        }

        $this->setUserEditAlert('Profil byl úspěšně upraven', 'success');

        // If editing currently logged in user -> update his identity
        if ($employeeId == $this->getUser()->getId()) {
            $this->updateUserIdentity($values);
        }
    }

    /**
     * Updates user identity
     *
     * @param $values : values from edit form
     */
    public function updateUserIdentity($values)
    {
        $currentIdentity = $this->getUser()->getIdentity();

        foreach ($values as $key => $value) {
            $currentIdentity->$key = $value;
        }
    }

    /**
     * Creates user password form
     *
     * @return UI\Form
     */
    protected function createComponentEditProfilePasswordForm()
    {
        $form = $this->userEditPasswordFormFactory->create();

        $form->addHidden('vysetrujici_id', $this->getParameter('id'));

        $form->addSubmit('sendEditPassword', 'Změnit heslo')
            ->setHtmlAttribute('class', 'form-control btn btn-warning');

        $form->onSuccess[] = [$this, 'editPasswordSuccess'];

        return $form;
    }

    /**
     * Edits user password in database and redirects back to profile edit
     *
     * @param $form
     * @param $values
     */
    public function editPasswordSuccess($form, $values)
    {
        $employeeId = $values->vysetrujici_id;
        $password = $values->password;
        $passwordConfirm = $values->passwordConfirm;

        if ($password !== $passwordConfirm || !PasswordValidator::isValidPassword($password)) {
            $this->setUserEditPasswordAlert('Hesla se neshodují, nebo heslo nesplňuje požadavky!', 'danger');
            return;
        }

        $passHash = Nette\Security\Passwords::hash($password);
        $result = $this->employeeModel->updateEmployeePasswordById($employeeId, $passHash);

        // Check for errors, If result = 0 -> zero affected rows -> error
        if ($result == 0) {
            $this->setUserEditPasswordAlert('Nastala chyba při změně hesla. Zkuste prosím formulář odeslat znovu.', 'danger');
            return;
        }

        $this->setUserEditPasswordAlert('Heslo bylo úspěšně změněno', 'success');
    }
}
