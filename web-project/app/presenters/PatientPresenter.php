<?php
/**
 * Created by PhpStorm.
 * User: honza
 * Date: 2. 7. 2018
 * Time: 15:48
 */

namespace App\Presenters;

use App\Model\DnaModel;
use App\Model\PatientModel;
use App\Model\SampleTypeModel;
use App\Utils\DataGrid\DataGrid;
use Nette;
use Nette\Application\UI;
use App\Components\PatientEditForm;
use Nette\Database\Table\ActiveRow;
use Nette\Database\Table\Selection;

class PatientPresenter extends Nette\Application\UI\Presenter
{

    /**
     * @var PatientModel @inject
     */
    public $patientModel;

    /**
     * @var DnaModel @inject
     */
    public $dnaModel;

    /** @var SampleTypeModel @inject */
    public $sampleTypeModel;

    /** @var PatientEditForm @inject */
    public $patientEditFormFactory;

    public function startup()
    {
        parent::startup();
        if (!$this->user->isInRole('admin') && !$this->user->isInRole('user')) {
            throw new \Nette\Application\ForbiddenRequestException();
        }
    }

    /**
     * Creates patient data grid
     *
     * @return DataGrid
     * @throws \Ublaboo\DataGrid\Exception\DataGridException
     */
    public function createComponentPatientDataGrid(){
        $grid = new DataGrid(null, 'patientDataGrid');

        $grid->setPrimaryKey(PatientModel::COL_ID);
        $grid->setDataSource($this->patientModel->database->table(PatientModel::TABLE_NAME));

        // Columns
        $grid->addColumnText(PatientModel::COL_ID, 'ID')
            ->setDefaultHide();
        $grid->addColumnText(PatientModel::COL_LAST_NAME, 'Jméno')
            ->setSortable();
        $grid->addColumnText(PatientModel::COL_FIRST_NAME, 'Příjmení')
            ->setSortable();
        $grid->addColumnText(PatientModel::COL_PIN, 'Rodné číslo')
            ->setRenderer(function ($item)
            {
                /** @var ActiveRow $item */
                $pin = PatientModel::COL_PIN;
                return substr_replace($item->$pin, '/', 6, 0);
            });

        // Filters
        $grid->addFilterText(PatientModel::COL_LAST_NAME, 'Příjmeni');
        $grid->addFilterText(PatientModel::COL_FIRST_NAME, 'Jméno');
        $grid->addFilterText(PatientModel::COL_PIN, 'Rodné číslo')
            ->setCondition(function ($selection, $value)
            {
                /** @var Selection $selection */
                $value = substr_replace($value, '', 6,1);
                return $selection->where(PatientModel::COL_PIN . ' LIKE ', '%' . $value . '%');
            });

        $grid->addAction('addDna', null,'Dna:add', ['id' => PatientModel::COL_ID])
            ->setTitle('Přidat DNA')
            ->setIcon('plus')
            ->setClass('success');

        $grid->addAction('detail', null, 'Patient:detail', ['id' => PatientModel::COL_ID])
            ->setTitle('Zobrazit detail pacienta')
            ->setIcon('pencil-alt')
            ->setClass('success');

        return $grid;
    }

    /**
     * @param $id
     */
    public function renderDetail($id){
        $this->template->patient = $this->patientModel->getPatientById($id)->fetch();
    }

    /**
     * @param $id
     */
    public function actionDeletePatient($id){

    }

    /**
     * @return UI\Form
     */
    protected function createComponentPatientNewForm(){
        $form = $this->patientEditFormFactory->create();

        $form->addSubmit('send', 'Přidat pacienta')
            ->setHtmlAttribute('class', 'form-control btn btn-success');

        $form->onSuccess[] = [$this, 'newPatientSuccess'];

        return $form;
    }

    /**
     * @return UI\Form
     */
    protected function createComponentPatientEditForm(){
        $form = $this->patientEditFormFactory->create();

        $form->addSubmit('send', 'Uložit nové údaje')
            ->setHtmlAttribute('class', 'form-control btn btn-success');

        $form->onSuccess[] = [$this, 'editPatientSuccess'];

        $data = $this->patientModel->getPatientById($this->getParameter('id'))->fetch();
        $form->setDefaults($data);

        return $form;
    }

    /**
     * @param $form
     * @param $values
     * @throws Nette\Application\AbortException
     */
    public function newPatientSuccess($form, $values){
        $this->patientModel->insertPatient($values);
        $this->flashMessage('Pacient byl přidán.', 'success');
        $this->redirect('Patient:List');
    }

    /**
     * @param $form
     * @param $values
     * @throws Nette\Application\AbortException
     */
    public function editPatientSuccess($form, $values){
        $this->patientModel->updatePatientById($this->getParameter('id'), $values);
        $this->flashMessage('Údaje byly aktualizovány.', 'success');
        $this->redirect('Patient:detail', $this->getParameter('id'));
    }

    /**
     * Creates patient DNA list data grid
     *
     * @return DataGrid
     * @throws \Ublaboo\DataGrid\Exception\DataGridException
     */
    public function createComponentDnaListDatagrid(){
        $grid = new DataGrid(null, 'dnaListDatagrid');

        $patientId = $this->getParameter('id');
        $sampleTypeModel = $this->sampleTypeModel;

        $grid->setPrimaryKey(DnaModel::COL_ID);
        $grid->setDataSource($this->dnaModel->getDnaByPatientId($patientId));

        // Columns
        $grid->addColumnText(DnaModel::COL_ID, 'ID')
            ->setDefaultHide();
        $grid->addColumnText(DnaModel::COL_SAMPLE_NUMBER, 'Číslo vzorku');

        // Render name instead of enum
        $grid->addColumnText(DnaModel::COL_SAMPLE_TYPE_ID, 'Typ vzorku')
            ->setRenderer(function ($item) use ($sampleTypeModel)
            {
                /** @var ActiveRow $item */
                $name = $sampleTypeModel->getSampleTypeById($item[DnaModel::COL_SAMPLE_TYPE_ID])->fetch();
                return $name[SampleTypeModel::COL_NAME];
            });

        $grid->addColumnText(DnaModel::COL_CAUSALITY, 'Kauzalita');
        $grid->addColumnDateTime(DnaModel::COL_DATE, 'Datum')
            ->setFormat('d.m.Y');

        // Actions
        $grid->addAction('add', null, 'Examination:add', ['id' => DnaModel::COL_ID])
            ->setTitle('Přidat vyšetření')
            ->setIcon('plus')
            ->setClass('success');

        $grid->addAction('detail', null, 'Dna:detail', ['id' => DnaModel::COL_ID])
            ->setTitle('Detail vzorku')
            ->setIcon('pencil-alt')
            ->setClass('success');

        // Filters
        $grid->addFilterText(DnaModel::COL_SAMPLE_NUMBER, 'Číslo vzorku');
        $grid->addFilterText(DnaModel::COL_CAUSALITY, 'Kauzalita');
        $grid->addFilterDateRange(DnaModel::COL_DATE, 'Datum')
            ->setFormat('j. n. Y', 'd.m.yyyy');

        // Search by ID instead of name
        $grid->addFilterText(DnaModel::COL_SAMPLE_TYPE_ID, 'Typ vzorku')
            ->setCondition(function ($selection, $value) use ($sampleTypeModel)
            {
                $sampleType = $sampleTypeModel->getSampleTypeLikeName($value)->fetchAll();

                $sampleIds = [];
                foreach ($sampleType as $sample){
                    $sampleIds[] = $sample[SampleTypeModel::COL_ID];
                }

                /** @var Selection $selection */
                $selection->where(DnaModel::COL_SAMPLE_TYPE_ID, $sampleIds);
            });

        return $grid;
    }
}