<?php

namespace App\Presenters;

use App\Model\EmployeeModel;
use App\Model\PatientModel;
use App\Model\RoleModel;
use App\Model\VariantModel;
use App\Utils\DataGrid\DataGrid;

class TestPresenter extends \Nette\Application\UI\Presenter
{
    /** @var PatientModel @inject */
    public $patientModel;

    /** @var VariantModel @inject */
    public $variantModel;

    /** @var EmployeeModel @inject */
    public $employeeModel;

    /** @var RoleModel @inject */
    public $roleModel;

    public function createComponentDatagrid(){
        $roles = $this->roleModel->getRoles()->fetchPairs(RoleModel::COL_ID, RoleModel::COL_NAME);
        $roles = [null => 'nepřiřazeno'] + $roles;

        $grid = new DataGrid();

        $grid->setPrimaryKey(EmployeeModel::COL_ID);

        $grid->setDataSource($this->employeeModel->listEmployees());
        $grid->setColumnsHideable();

        // Columns
        $grid->addColumnNumber(EmployeeModel::COL_ID, 'ID')->setDefaultHide();
        $grid->addColumnText(EmployeeModel::COL_LAST_NAME, 'Příjmení');
        $grid->addColumnText(EmployeeModel::COL_FIRST_NAME, 'Jméno');
        $grid->addColumnText(EmployeeModel::COL_LOGIN, 'Login');
        $grid->addColumnText(EmployeeModel::COL_ROLE, 'Role')
            ->setReplacement($roles);

        // Filters
        $grid->addFilterText(EmployeeModel::COL_LAST_NAME, 'Příjmení');
        $grid->addFilterText(EmployeeModel::COL_FIRST_NAME, 'Jméno');
        $grid->addFilterText(EmployeeModel::COL_LOGIN, 'Login');
        $grid->addFilterSelect(EmployeeModel::COL_ROLE, 'Role', $roles);

        return $grid;
    }

    public function addGenes(array $ids){
        var_dump($ids);
    }
}