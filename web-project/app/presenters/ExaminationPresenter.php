<?php
/**
 * Created by PhpStorm.
 * User: Fifal
 * Date: 27.08.2018
 * Time: 20:23
 */

namespace App\Presenters;


use App\Components\ExaminationForm;
use App\Components\VariantGrid;
use App\Model\DnaGenModel;
use App\Model\DnaModel;
use App\Model\EmployeeModel;
use App\Model\ExaminationModel;
use App\Model\GeneModel;
use App\Model\PanelGeneModel;
use App\Model\PanelModel;
use App\Model\PatientModel;
use App\Model\VariantExamModel;
use App\Model\VariantModel;
use App\Utils\DataGrid\DataGrid;
use Nette\Application\UI\Presenter;
use Nette\Database\Table\Selection;
use Tracy\Debugger;

class ExaminationPresenter extends Presenter
{
    /** @var ExaminationModel @inject */
    public $examinationModel;

    /** @var PanelModel @inject */
    public $panelModel;

    /** @var PanelGeneModel @inject */
    public $panelGeneModel;

    /** @var GeneModel @inject */
    public $geneModel;

    /** @var EmployeeModel @inject */
    public $employeeModel;

    /** @var ExaminationForm @inject */
    public $examFormFactory;

    /** @var VariantGrid @inject */
    public $variantGridFactory;

    /** @var VariantModel @inject */
    public $variantModel;

    /** @var DnaModel @inject */
    public $dnaModel;

    /** @var VariantExamModel @inject */
    public $variantExamModel;

    /** @var PatientModel @inject */
    public $patientModel;

    /** @var DnaGenModel @inject */
    public $dnaGeneModel;

    public function startup()
    {
        parent::startup();
        if (!$this->user->isInRole('admin') && !$this->user->isInRole('user')) {
            throw new \Nette\Application\ForbiddenRequestException();
        }
    }

    /**
     * Action for examination edit
     *
     * @param $id
     */
    public function actionEdit($id)
    {
        $this->template->genes = $this->getTemplateGenes($id);
    }

    /**
     * Returns names of genes in given panel
     *
     * @param null $id
     * @return string
     */
    public function getTemplateGenes($id = null)
    {
        if ($id === null)
        {
            $id = $this->getParameter('id');
        }

        $panel = $this->examinationModel->getExaminationById($id)->fetch();
        $geneIds = $this->panelGeneModel->getGenesIdsInPanel($panel[ExaminationModel::COL_PANEL_ID]);
        if (!empty($geneIds))
        {
            $genes = $this->geneModel->listGenes()
                ->where(GeneModel::COL_ID . ' IN(' . implode(',', $geneIds) . ')')->fetchPairs(GeneModel::COL_ID, GeneModel::COL_NAME);
        } else
        {
            $genes = [];
        }

        return implode(', ', $genes);
    }

    /**
     * Render for adding new examination to existing DNA sample
     *
     * @param $id
     */
    public function renderAdd($id)
    {
        if ($id !== null)
        {
            $dna = $this->dnaModel->getDnaById($id)->fetch();
            if (isset($dna[DnaModel::COL_PATIENT_ID]))
            {
                $patient = $this->patientModel->getPatientById($dna[DnaModel::COL_PATIENT_ID])->fetch();
            }

            if (isset($patient) && $patient)
            {
                $this->template->patient = $patient;
            }
        }
    }

    public function createComponentVariantExamGrid()
    {

        $id = $this->presenter->getParameter('id');

        $variantModel = $this->variantModel;
        $dnaGeneModel = $this->dnaGeneModel;
        $options = $this->dnaGeneModel->database->table(DnaGenModel::TABLE_NAME)->fetchPairs(DnaGenModel::COL_GEN_ID, DnaGenModel::COL_GEN_TYPE);

        $grid = new DataGrid(null, 'variantExamGrid');
        $grid->setPrimaryKey(VariantExamModel::COL_VARIANT_ID);

        $grid->addColumnText(VariantExamModel::COL_VARIANT_ID, 'Varianta ID')
            ->setDefaultHide();

        $grid->setDataSource($this->variantExamModel->database->table(VariantExamModel::TABLE_NAME)->where([VariantExamModel::COL_EXAMINATION_ID => $id]));

        $grid->addColumnLink(VariantModel::COL_HGVS, 'HGSV', '', VariantModel::COL_HGVS, ['id' => VariantExamModel::COL_VARIANT_ID])
            ->setRenderer(function ($item) use ($variantModel)
            {
                $variant = $variantModel->getVariantById($item[VariantExamModel::COL_VARIANT_ID])->fetch();

                return $variant[VariantModel::COL_HGVS];
            });

        $grid->addColumnLink(VariantModel::COL_RS, 'RS', '', VariantModel::COL_RS, ['id' => VariantExamModel::COL_VARIANT_ID])
            ->setRenderer(function ($item) use ($variantModel)
            {
                $variant = $variantModel->getVariantById($item[VariantExamModel::COL_VARIANT_ID])->fetch();

                return $variant[VariantModel::COL_RS];
            });

        $grid->addColumnLink(VariantModel::COL_CLINVAR_VERDICT, 'Clinvar význam', '', VariantModel::COL_CLINVAR_VERDICT, ['id' => VariantExamModel::COL_VARIANT_ID])
            ->setRenderer(function ($item) use ($variantModel)
            {
                $variant = $variantModel->getVariantById($item[VariantExamModel::COL_VARIANT_ID])->fetch();

                return $variant[VariantModel::COL_CLINVAR_VERDICT];
            });

        $grid->addColumnLink(DnaGenModel::COL_GEN_TYPE, 'Genotyp', '', DnaGenModel::COL_GEN_TYPE, ['id' => VariantExamModel::COL_DNA_GEN_ID])
            ->setRenderer(function ($item) use ($dnaGeneModel)
            {
                $dnaGene = $dnaGeneModel->getDnaGenById($item[VariantExamModel::COL_DNA_GEN_ID])->fetch();

                return $dnaGene[DnaGenModel::COL_GEN_TYPE];
            });

        $grid->addFilterText(VariantModel::COL_HGVS, 'HGVS')
            ->setCondition(function ($selection, $value) use ($variantModel)
            {

                $variants = $variantModel->getVariantLikeHGVS($value)->fetchAll();
                $variantIds = [];
                foreach ($variants as $variant)
                {
                    $variantIds[] = $variant[VariantModel::COL_ID];
                }

                if (count($variantIds) == 0)
                {
                    /** @var Selection $selection */
                    $selection->where(VariantExamModel::COL_VARIANT_ID, null);
                } else
                {
                    /** @var Selection $selection */
                    $selection->where(VariantExamModel::COL_VARIANT_ID, $variantIds);
                }

            });

        $grid->addFilterText(VariantModel::COL_CLINVAR_VERDICT, 'Clinvar význam')
            ->setCondition(function ($selection, $value) use ($variantModel)
            {

                $variants = $variantModel->getVariantLikeClinVerdict($value)->fetchAll();
                $variantIds = [];
                foreach ($variants as $variant)
                {
                    $variantIds[] = $variant[VariantModel::COL_ID];
                }

                if (count($variantIds) == 0)
                {
                    /** @var Selection $selection */
                    $selection->where(VariantExamModel::COL_VARIANT_ID, null);
                } else
                {
                    /** @var Selection $selection */
                    $selection->where(VariantExamModel::COL_VARIANT_ID, $variantIds);
                }

            });

        $grid->addFilterText(VariantModel::COL_RS, 'RS')
            ->setCondition(function ($selection, $value) use ($variantModel)
            {

                $variants = $variantModel->getVariantLikeRS($value)->fetchAll();
                $variantIds = [];
                foreach ($variants as $variant)
                {
                    $variantIds[] = $variant[VariantModel::COL_ID];
                }

                if (count($variantIds) == 0)
                {
                    /** @var Selection $selection */
                    $selection->where(VariantExamModel::COL_VARIANT_ID, null);
                } else
                {
                    /** @var Selection $selection */
                    $selection->where(VariantExamModel::COL_VARIANT_ID, $variantIds);
                }

            });

        $grid->addFilterText(DnaGenModel::COL_GEN_TYPE, 'Genotyp')
            ->setCondition(function ($selection, $value) use ($dnaGeneModel)
            {

                $dna = $dnaGeneModel->getDnaGenLikeGenotyp($value)->fetchAll();
                $dnaIds = [];
                foreach ($dna as $d)
                {
                    $dnaIds[] = $d[DnaGenModel::COL_GEN_ID];
                }

                if (count($dnaIds) == 0)
                {
                    /** @var Selection $selection */
                    $selection->where(VariantExamModel::COL_DNA_GEN_ID, null);
                } else
                {
                    /** @var Selection $selection */
                    $selection->where(VariantExamModel::COL_DNA_GEN_ID, $dnaIds);
                }

            });

        //adds filter button "THE BEST FIX"
        $grid->addAction('FILTER-FIX', null, null);

        $grid->addGroupAction('Změnit genotyp', $options)->onSelect[] = [$this, 'handleChangeGenotype'];
        $grid->addGroupAction('Odebrat variantu z vyšetření')->onSelect[] = [$this, 'handleDeleteFromExam'];

        return $grid;
    }

    /**
     * Handles deletion and genotype changing of variant in examination
     *      - if option is null it will delete variant from examination
     *      - if option is set it will change genotype to given option
     * @param $variants
     * @param null $option
     * @throws \Nette\Application\AbortException
     */
    public function changeVariantExam($variants, $option = null)
    {
        if (!$this->isAjax())
        {
            $this->redirect('this');
        }

        $id = $this->getPresenter()->getParameter('id');

        if (!is_array($variants))
        {
            $variants = array($variants);
        }

        foreach ($variants as $variant)
        {
            $where = [VariantExamModel::COL_VARIANT_ID => $variant, VariantExamModel::COL_EXAMINATION_ID => $id];

            // If option is null delete, otherwise change genotype
            if ($option === null)
            {
                $this->variantExamModel->database->table(VariantExamModel::TABLE_NAME)
                    ->where($where)
                    ->delete();
            } else
            {
                $this->variantExamModel->database->table(VariantExamModel::TABLE_NAME)
                    ->where($where)
                    ->update([VariantExamModel::COL_DNA_GEN_ID => $option]);
            }
        }

        $this['variantExamGrid']->reload();
        $this->redrawControl('variantExamGridSnippet');
    }

    /**
     * Handles genotype change for variant in examination
     * @param array $variants
     * @param $option from table dna_gen
     * @throws \Nette\Application\AbortException
     */
    public function handleChangeGenotype(array $variants, $option)
    {
        $this->changeVariantExam($variants, $option);
    }

    /**
     * Handles deletion of variants from examination
     * @param array $variants
     * @throws \Nette\Application\AbortException
     */
    public function handleDeleteFromExam(array $variants)
    {
        $this->changeVariantExam($variants);
    }

    /**
     * Creates Examination list data grid
     *
     * @return DataGrid
     * @throws \Ublaboo\DataGrid\Exception\DataGridException
     */
    public function createComponentExamDataGrid()
    {
        $panelModel = $this->panelModel;
        $employeeModel = $this->employeeModel;
        $dnaModel = $this->dnaModel;

        $grid = new DataGrid(null, 'examDataGrid');
        $grid->setPrimaryKey(ExaminationModel::COL_ID);
        $grid->setDataSource($this->examinationModel->listExaminations());

        // Columns
        $grid->addColumnText(ExaminationModel::COL_ID, 'ID')
            ->setDefaultHide();

        $grid->addColumnText(ExaminationModel::COL_PANEL_ID, 'Název panelu')
            ->setRenderer(function ($item) use ($panelModel)
            {
                $panel = $panelModel->getPanelById($item[ExaminationModel::COL_PANEL_ID])->fetch();

                if ($panel)
                {
                    return $panel[PanelModel::COL_NAME];
                } else
                {
                    return $item;
                }
            })->setSortable();

        $grid->addColumnText(ExaminationModel::COL_DNA_ID, 'Vzorek DNA')
            ->setRenderer(function ($item) use ($dnaModel)
            {
                $dna = $dnaModel->getDnaById($item[ExaminationModel::COL_DNA_ID])->fetch();

                if ($dna)
                {
                    return $dna[DnaModel::COL_SAMPLE_NUMBER];
                } else
                {
                    return $item;
                }
            })->setSortable();

        $grid->addColumnDateTime(ExaminationModel::COL_TIMESTAMP, 'Datum');

        $grid->addColumnText(ExaminationModel::COL_EMPLOYEE_ID, 'Vyšetřující')
            ->setRenderer(function ($item) use ($employeeModel)
            {
                $employee = $this->employeeModel->getEmployeeById($item[ExaminationModel::COL_EMPLOYEE_ID])->fetch();

                if ($employee)
                {
                    return $employee[EmployeeModel::COL_FIRST_NAME] . ' ' . $employee[EmployeeModel::COL_LAST_NAME];
                } else
                {
                    return $item;
                }
            })->setSortable();

        //filters
        $grid->addFilterText(ExaminationModel::COL_DNA_ID, 'Vzorek DNA')
            ->setCondition(function ($selection, $value) use ($dnaModel)
            {

                $dna = $dnaModel->getDnaLikeSampleNumber($value)->fetchAll();
                $dnaIds = [];
                foreach ($dna as $d)
                {
                    $dnaIds[] = $d[DnaModel::COL_ID];
                }

                if (count($dnaIds) == 0)
                {
                    /** @var Selection $selection */
                    $selection->where(ExaminationModel::COL_DNA_ID, null);
                } else
                {
                    /** @var Selection $selection */
                    $selection->where(ExaminationModel::COL_DNA_ID, $dnaIds);
                }

            });

        $grid->addFilterText(ExaminationModel::COL_PANEL_ID, 'Název panelu')
            ->setCondition(function ($selection, $value) use ($panelModel)
            {

                $panels = $panelModel->getPanelLikeName($value)->fetchAll();
                $panelIds = [];
                foreach ($panels as $panel)
                {
                    $panelIds[] = $panel[PanelModel::COL_ID];
                }

                if (count($panelIds) == 0)
                {
                    /** @var Selection $selection */
                    $selection->where(ExaminationModel::COL_PANEL_ID, null);
                } else
                {
                    /** @var Selection $selection */
                    $selection->where(ExaminationModel::COL_PANEL_ID, $panelIds);
                }

            });

        $grid->addFilterDateRange(ExaminationModel::COL_TIMESTAMP, 'Datum');

        // Actions
        $grid->addAction('edit', null, 'Examination:edit', ['id' => ExaminationModel::COL_ID])
            ->setTitle('Editovat vyšetření')
            ->setIcon('pencil-alt')
            ->setClass('success');

        $grid->addAction('delete', null, 'deleteExamination!', ['id' => ExaminationModel::COL_ID])
            ->setTitle('smazat vyšetření')
            ->setIcon('trash')
            ->setClass('danger ajax')
            ->setConfirm('Opravdu chcete vyšetření %s odstranit?', ExaminationModel::COL_ID);

        return $grid;
    }

    /**
     * Handles Examination deletion
     *
     * @param $id
     * @throws \Nette\Application\AbortException
     */
    public function handleDeleteExamination($id)
    {
        if (!$this->isAjax())
        {
            $this->redirect('this');
        }

        if ($this->examinationModel->deleteExaminationById($id))
        {
            $this->flashMessage('Vyšetření bylo úspěšně odstraněno.', 'success');
        } else
        {
            $this->flashMessage('Vyšetření se nepodařilo smazat.', 'error');
        }

        $this->redrawControl('flashes');
        $this['examDataGrid']->reload();
    }

    /**
     * Creates form for Examination adding
     *
     * @return \App\Components\BootstrapForm
     */
    public function createComponentExamAddForm()
    {
        $form = $this->examFormFactory->create();

        // Add employee ID into the form
        $form[ExaminationModel::COL_EMPLOYEE_ID]->setValue($this->getUser()->getId());

        $form->addSubmit('send', 'Přidat vyšetření')
            ->setHtmlAttribute('class', 'btn btn-success');

        $form->onSuccess[] = [$this, 'examAddSuccess'];
        return $form;
    }

    /**
     * Handles add form success
     *
     * @param $form
     * @param $values
     * @throws \Nette\Application\AbortException
     */
    public function examAddSuccess($form, $values)
    {

        $result = $this->examinationModel->insertExamination(
            [
                ExaminationModel::COL_EMPLOYEE_ID => $values[ExaminationModel::COL_EMPLOYEE_ID],
                ExaminationModel::COL_PANEL_ID => $values[ExaminationModel::COL_PANEL_ID],
                ExaminationModel::COL_DNA_ID => $values[ExaminationModel::COL_DNA_ID],
            ]
        );

        $examination_id = $this->examinationModel->getExaminationByParameters($values[ExaminationModel::COL_DNA_ID],
            $values[ExaminationModel::COL_EMPLOYEE_ID], $values[ExaminationModel::COL_PANEL_ID])->fetch()->vysetreni_id;

        if ($result)
        {
            $this->redirect('Examination:edit', $examination_id);
        } else
        {
            $this->flashMessage('Vyšetření se nepodařilo přidat, zkuste to prosím znovu.', 'error');
            $this->redrawControl('flashes');
        }
    }

    /**
     * Creates Edit examination form
     *
     * @throws \Nette\Application\AbortException
     */
    public function createComponentExamEditForm()
    {
        $id = $this->getParameter('id');
        if ($id === null)
        {
            $this->redirect('Examination:add');
        }

        $form = $this->examFormFactory->createEdit();

        // Add employee ID into the form
        $form[ExaminationModel::COL_EMPLOYEE_ID]->setValue($this->getUser()->getId());

        $form->addSubmit('send', 'Upravit vyšetření')
            ->setHtmlAttribute('class', 'btn btn-success ajax');

        $form->getElementPrototype()->onSubmit = 'return Nette.validateForm(this) ? confirm(\'Opravdu chcete upravit panel vyšetření? Pokud vyberete nový panel ve kterém nejsou geny, které jsou aktuálně přiřazené k vyšetření, budou informace odstraněny. \') : false';

        $form->onSuccess[] = [$this, 'handleExamEdit'];

        // Set default values
        $exam = $this->examinationModel->getExaminationById($id)->fetch();
        if ($exam)
        {
            $form->setDefaults($exam);
        }

        return $form;
    }

    /**
     * Creates form for assigning new examination to existing patient and his DNA sample
     *
     * @return \App\Components\BootstrapForm
     */
    public function createComponentExamAddToPatientForm()
    {
        $form = $this->examFormFactory->createEdit();

        $form->addHidden(ExaminationModel::COL_DNA_ID, $this->presenter->getParameter('id'));

        // Add employee ID into the form
        $form[ExaminationModel::COL_EMPLOYEE_ID]->setValue($this->getUser()->getId());

        $form->addSubmit('send', 'Přidat vyšetření')
            ->setHtmlAttribute('class', 'btn btn-success');

        $form->onSuccess[] = [$this, 'examAddSuccess'];

        return $form;
    }

    /**
     * Creates grid with variants
     *
     * @return DataGrid
     * @throws \Ublaboo\DataGrid\Exception\DataGridException
     * @throws \Nette\Application\AbortException
     */
    public function createComponentVariantGrid()
    {
        $id = $this->getParameter('id');
        if ($id === null)
        {
            $this->redirect('Examination:add');
        }

        $exam = $this->examinationModel->getExaminationById($id)->fetch();
        if ($exam)
        {
            $grid = $this->variantGridFactory->create($exam[ExaminationModel::COL_PANEL_ID]);
            $grid->addGroupAction('Přiřadit variantu k vyšetření')->onSelect[] = [$this, 'handleAddVariantToExam'];

            $genes = $this->panelGeneModel->getGenesIdsInPanel($exam[ExaminationModel::COL_PANEL_ID]);

            //adds filter button "THE BEST FIX"
            $grid->addAction('FILTER-FIX', null, null);

            if (empty($genes))
            {
                $this->flashMessage('V panelu nebyl nalezen žádný gen!', 'danger');
            }
        } else
        {
            $grid = $this->variantGridFactory->create(null);
        }

        return $grid;
    }

    /**
     * Handles adding IDs to examination
     *
     * @param array $ids : variant IDs
     * @throws \Nette\Application\AbortException
     */
    public function handleAddVariantToExam($ids)
    {
        if (!$this->isAjax())
        {
            $this->redirect('this');
        }

        if (!is_array($ids))
        {
            $ids[] = $ids;
        }

        $examId = $this->presenter->getParameter('id');

        $count = 0;
        foreach ($ids as $id)
        {
            try
            {
                $result = $this->variantExamModel->insertVariantExamination(
                    [
                        VariantExamModel::COL_VARIANT_ID => $id,
                        VariantExamModel::COL_EXAMINATION_ID => $examId,
                        VariantExamModel::COL_DNA_GEN_ID => 1
                    ]
                );

                if ($result)
                {
                    $count++;
                }
            } catch (\Exception $exception)
            {
                Debugger::log("Nepodařilo se přidat variantu ($id) k vyšetření. ", 'database.error');
                continue;
            }
        }

        $this->redrawControl('flashes');
        $this['variantGrid']->reload();
        $this->redrawControl('variantGridSnippet');
        $this['variantExamGrid']->reload();
    }

    /**
     * Handles panel change
     *
     * @param $form
     * @param $values
     * @throws \Nette\Application\AbortException
     */
    public function handleExamEdit($form, $values)
    {
        if (!$this->isAjax())
        {
            $this->redirect('this');
        }

        $examinationId = $this->getPresenter()->getParameter('id');
        $panelGeneIds = $this->panelGeneModel->getGenesIdsInPanel($values[ExaminationModel::COL_PANEL_ID]);
        $variants = $this->variantExamModel->getVarExamByExamId($examinationId);

        foreach ($variants as $variant)
        {
            $variantId = $variant->{VariantExamModel::COL_VARIANT_ID};
            $variantRow = $this->variantModel->getVariantById($variantId)->fetch();
            if (!$variantRow)
            {
                continue;
            }

            if (!in_array($variantRow->{VariantModel::COL_GEN_ID}, $panelGeneIds))
            {
                $this->variantExamModel->deleteVarExamByPK($variantRow->{VariantModel::COL_ID}, $examinationId);
            }
        }


        $result = $this->examinationModel->updateExaminationById($this->getParameter('id'),
            [
                ExaminationModel::COL_PANEL_ID => $values[ExaminationModel::COL_PANEL_ID]
            ]
        );

        if ($result)
        {
            $this->flashMessage('Panel byl změněn.', 'success');
        } else
        {
            $this->flashMessage('Panel se nepodařilo změnit.', 'error');
        }

        $this->redrawControl('flashes');
        $this['variantGrid']->reload();
        $this->redrawControl('variantGridSnippet');
        $this['variantExamGrid']->reload();
        $this->redrawControl('genesSnippet');
    }
}