<?php
/**
 * Created by PhpStorm.
 * User: Fifal
 * Date: 04.07.2018
 * Time: 18:30
 */

namespace App\Utils;


class ImportSql
{
    /**
     * Returns database creation script
     *
     * @param $databaseName
     * @return string
     */
    public static function getSqlScript($databaseName)
    {
        $sqlScript = 'ALTER DATABASE ' . $databaseName . ' SET COMPATIBILITY_LEVEL = 140

        IF (1 = FULLTEXTSERVICEPROPERTY(\'IsFullTextInstalled\'))
        begin
        EXEC ' . $databaseName . '.[dbo].[sp_fulltext_database] @action = \'enable\'
        end
        
        ALTER DATABASE ' . $databaseName . ' SET ANSI_NULL_DEFAULT OFF 
        
        ALTER DATABASE ' . $databaseName . ' SET ANSI_NULLS OFF 
        
        ALTER DATABASE ' . $databaseName . ' SET ANSI_PADDING OFF 
        
        ALTER DATABASE ' . $databaseName . ' SET ANSI_WARNINGS OFF 
        
        ALTER DATABASE ' . $databaseName . ' SET ARITHABORT OFF 
        
        ALTER DATABASE ' . $databaseName . ' SET AUTO_CLOSE OFF 
        
        ALTER DATABASE ' . $databaseName . ' SET AUTO_SHRINK OFF 
        
        ALTER DATABASE ' . $databaseName . ' SET AUTO_UPDATE_STATISTICS ON 
        
        ALTER DATABASE ' . $databaseName . ' SET CURSOR_CLOSE_ON_COMMIT OFF 
        
        ALTER DATABASE ' . $databaseName . ' SET CURSOR_DEFAULT  GLOBAL 
        
        ALTER DATABASE ' . $databaseName . ' SET CONCAT_NULL_YIELDS_NULL OFF 
        
        ALTER DATABASE ' . $databaseName . ' SET NUMERIC_ROUNDABORT OFF 
        
        ALTER DATABASE ' . $databaseName . ' SET QUOTED_IDENTIFIER OFF 
        
        ALTER DATABASE ' . $databaseName . ' SET RECURSIVE_TRIGGERS OFF 
        
        ALTER DATABASE ' . $databaseName . ' SET  DISABLE_BROKER 
        
        ALTER DATABASE ' . $databaseName . ' SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
        
        ALTER DATABASE ' . $databaseName . ' SET DATE_CORRELATION_OPTIMIZATION OFF 
        
        ALTER DATABASE ' . $databaseName . ' SET TRUSTWORTHY OFF 
        
        ALTER DATABASE ' . $databaseName . ' SET ALLOW_SNAPSHOT_ISOLATION OFF 
        
        ALTER DATABASE ' . $databaseName . ' SET PARAMETERIZATION SIMPLE 
        
        ALTER DATABASE ' . $databaseName . ' SET READ_COMMITTED_SNAPSHOT OFF 
        
        ALTER DATABASE ' . $databaseName . ' SET HONOR_BROKER_PRIORITY OFF 
        
        ALTER DATABASE ' . $databaseName . ' SET RECOVERY FULL 
        
        ALTER DATABASE ' . $databaseName . ' SET  MULTI_USER 
        
        ALTER DATABASE ' . $databaseName . ' SET PAGE_VERIFY CHECKSUM  
        
        ALTER DATABASE ' . $databaseName . ' SET DB_CHAINING OFF 
        
        ALTER DATABASE ' . $databaseName . ' SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
        
        ALTER DATABASE ' . $databaseName . ' SET TARGET_RECOVERY_TIME = 60 SECONDS 
        
        ALTER DATABASE ' . $databaseName . ' SET DELAYED_DURABILITY = DISABLED 
        
        EXEC sys.sp_db_vardecimal_storage_format N\'genomic_database_dev\', N\'ON\'
        
        ALTER DATABASE ' . $databaseName . ' SET QUERY_STORE = OFF
        
        USE ' . $databaseName . '
        
        ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
        
        ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
        
        ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
        
        ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
        
        ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
        
        ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
        
        ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
        
        ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
        
        ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
        
       /****** Object:  Table [dbo].[admin_reset]    Script Date: 04.07.2018 18:25:36 ******/
       SET ANSI_NULLS ON
        
       SET QUOTED_IDENTIFIER ON
        
       CREATE TABLE [dbo].[admin_reset](
            [admin_reset_heslo] [nvarchar](60) NOT NULL,
            [admin_reset_pouzito] [bit] NOT NULL
        ) ON [PRIMARY]
        
        /****** Object:  Table [dbo].[dna]    Script Date: 25.09.2018 21:13:08 ******/
        SET ANSI_NULLS ON
        
        SET QUOTED_IDENTIFIER ON
        
        CREATE TABLE [dbo].[dna](
            [dna_id] [int] IDENTITY(1,1) NOT NULL,
            [dna_typ_vzorku_id] [int] NULL,
            [dna_pacient_id] [int] NOT NULL,
            [dna_cislo_vzorku] [nvarchar](50) NOT NULL,
            [dna_gen] [nvarchar](100) NULL,
            [dna_datum] [date] NULL,
            [dna_kauzalita] [nvarchar](50) NULL,
            [dna_dna_gen_id] [int] NULL,
         CONSTRAINT [PK_dna] PRIMARY KEY CLUSTERED 
        (
            [dna_id] ASC
        )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]
        
        /****** Object:  Table [dbo].[dna_gen]    Script Date: 25.09.2018 21:13:08 ******/
        SET ANSI_NULLS ON
        
        SET QUOTED_IDENTIFIER ON
        
        CREATE TABLE [dbo].[dna_gen](
            [dna_gen_id] [int] IDENTITY(1,1) NOT NULL,
            [dna_gen_typ] [nvarchar](50) NOT NULL,
         CONSTRAINT [PK_dna_gen] PRIMARY KEY CLUSTERED 
        (
            [dna_gen_id] ASC
        )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]
        
        /****** Object:  Table [dbo].[gen]    Script Date: 25.09.2018 21:13:09 ******/
        SET ANSI_NULLS ON
        
        SET QUOTED_IDENTIFIER ON
        
        CREATE TABLE [dbo].[gen](
            [gen_id] [int] IDENTITY(1,1) NOT NULL,
            [gen_nazev] [nvarchar](50) NOT NULL,
            [gen_popis] [nvarchar](300) NULL,
            [gen_synonyma] [nvarchar](200) NULL,
            [gen_reference] [nvarchar](200) NULL,
            [gen_datum_od] [date] NULL,
            [gen_datum_do] [date] NULL,
         CONSTRAINT [PK_gen] PRIMARY KEY CLUSTERED 
        (
            [gen_id] ASC
        )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]
        
        /****** Object:  Table [dbo].[pacient]    Script Date: 25.09.2018 21:13:09 ******/
        SET ANSI_NULLS ON
        
        SET QUOTED_IDENTIFIER ON
        
        CREATE TABLE [dbo].[pacient](
            [pacient_id] [int] IDENTITY(1,1) NOT NULL,
            [pacient_jmeno] [nvarchar](50) NULL,
            [pacient_prijmeni] [nvarchar](50) NULL,
            [pacient_rc] [nvarchar](10) NULL,
         CONSTRAINT [PK_pacient] PRIMARY KEY CLUSTERED 
        (
            [pacient_id] ASC
        )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]
        
        /****** Object:  Table [dbo].[panel]    Script Date: 25.09.2018 21:13:09 ******/
        SET ANSI_NULLS ON
        
        SET QUOTED_IDENTIFIER ON
        
        CREATE TABLE [dbo].[panel](
            [panel_id] [int] IDENTITY(1,1) NOT NULL,
            [panel_nazev] [nvarchar](200) NOT NULL,
            [panel_poznamka] [nvarchar](200) NULL,
         CONSTRAINT [PK_panel] PRIMARY KEY CLUSTERED 
        (
            [panel_id] ASC
        )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]
        
        /****** Object:  Table [dbo].[panel_gen]    Script Date: 25.09.2018 21:13:09 ******/
        SET ANSI_NULLS ON
        
        SET QUOTED_IDENTIFIER ON
        
        CREATE TABLE [dbo].[panel_gen](
            [panel_gen_panel_id] [int] NOT NULL,
            [panel_gen_gen_id] [int] NOT NULL,
         CONSTRAINT [PK_panel_gen] PRIMARY KEY CLUSTERED 
        (
            [panel_gen_panel_id] ASC,
            [panel_gen_gen_id] ASC
        )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]
        
        /****** Object:  Table [dbo].[role]    Script Date: 25.09.2018 21:13:09 ******/
        SET ANSI_NULLS ON
        
        SET QUOTED_IDENTIFIER ON
        
        CREATE TABLE [dbo].[role](
            [role_id] [int] IDENTITY(1,1) NOT NULL,
            [role_nazev] [nvarchar](50) NOT NULL,
         CONSTRAINT [PK_vysetrujici_role] PRIMARY KEY CLUSTERED 
        (
            [role_id] ASC
        )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]
        
        /****** Object:  Table [dbo].[typ_vzorku]    Script Date: 25.09.2018 21:13:09 ******/
        SET ANSI_NULLS ON
        
        SET QUOTED_IDENTIFIER ON
        
        CREATE TABLE [dbo].[typ_vzorku](
            [typ_vzorku_id] [int] NOT NULL,
            [typ_vzorku_nazev] [nvarchar](50) NULL,
         CONSTRAINT [PK_typ_vzorku] PRIMARY KEY CLUSTERED 
        (
            [typ_vzorku_id] ASC
        )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]
        
        /****** Object:  Table [dbo].[varianta]    Script Date: 25.09.2018 21:13:09 ******/
        SET ANSI_NULLS ON
        
        SET QUOTED_IDENTIFIER ON
        
        CREATE TABLE [dbo].[varianta](
            [varianta_id] [int] IDENTITY(1,1) NOT NULL,
            [varianta_gen_id] [int] NULL,
            [varianta_hgvs] [nvarchar](50) NOT NULL,
            [varianta_rs] [int] NULL,
            [varianta_clinvar_datum] [datetime] NULL,
            [varianta_clinvar_protein] [nvarchar](50) NULL,
            [varianta_clinvar_vyznam] [nvarchar](50) NULL,
            [varianta_poznamka] [nvarchar](300) NULL,
            [varianta_datum_od] [date] NULL,
            [varianta_datum_do] [date] NULL,
            [varianta_vyznam] [nvarchar](50) NULL,
         CONSTRAINT [PK_varianta] PRIMARY KEY CLUSTERED 
        (
            [varianta_id] ASC
        )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]
        
        /****** Object:  Table [dbo].[varianta_vysetreni]    Script Date: 25.09.2018 21:13:09 ******/
        SET ANSI_NULLS ON
        
        SET QUOTED_IDENTIFIER ON
        
        CREATE TABLE [dbo].[varianta_vysetreni](
            [varianta_vysetreni_varianta_id] [int] NOT NULL,
            [varianta_vysetreni_vysetreni_id] [int] NOT NULL,
            [varianta_vysetreni_dna_gen_id] [int] NOT NULL,
         CONSTRAINT [PK_varianta_vysetreni] PRIMARY KEY CLUSTERED 
        (
            [varianta_vysetreni_varianta_id] ASC,
            [varianta_vysetreni_vysetreni_id] ASC
        )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]
        
        /****** Object:  Table [dbo].[vysetreni]    Script Date: 25.09.2018 21:13:09 ******/
        SET ANSI_NULLS ON
        
        SET QUOTED_IDENTIFIER ON
        
        CREATE TABLE [dbo].[vysetreni](
            [vysetreni_id] [int] IDENTITY(1,1) NOT NULL,
            [vysetreni_vysetrujici_id] [int] NOT NULL,
            [vysetreni_dna_id] [int] NOT NULL,
            [vysetreni_panel_id] [int] NOT NULL,
            [vysetreni_datum] [date] NOT NULL,
         CONSTRAINT [PK_vysetreni] PRIMARY KEY CLUSTERED 
        (
            [vysetreni_id] ASC
        )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]
        
        /****** Object:  Table [dbo].[vysetrujici]    Script Date: 25.09.2018 21:13:09 ******/
        SET ANSI_NULLS ON
        
        SET QUOTED_IDENTIFIER ON
        
        CREATE TABLE [dbo].[vysetrujici](
            [vysetrujici_id] [int] IDENTITY(1,1) NOT NULL,
            [vysetrujici_login] [nvarchar](50) NULL,
            [vysetrujici_password] [nvarchar](max) NULL,
            [vysetrujici_jmeno] [nvarchar](50) NULL,
            [vysetrujici_prijmeni] [nvarchar](50) NULL,
            [vysetrujici_titul_pred] [nvarchar](50) NULL,
            [vysetrujici_titul_za] [nvarchar](50) NULL,
            [vysetrujici_role] [int] NULL,
         CONSTRAINT [PK_vysetrujici] PRIMARY KEY CLUSTERED 
        (
            [vysetrujici_id] ASC
        )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
        
        ALTER TABLE [dbo].[varianta] ADD  CONSTRAINT [DF_varianta_varianta_datum_od]  DEFAULT (getdate()) FOR [varianta_datum_od]
        
        ALTER TABLE [dbo].[vysetrujici] ADD  CONSTRAINT [DF__vysetruji__vyset__71D1E811]  DEFAULT (user_name()) FOR [vysetrujici_role]
        
        ALTER TABLE [dbo].[dna]  WITH CHECK ADD  CONSTRAINT [FK_dna_dna_gen_id] FOREIGN KEY([dna_dna_gen_id])
        REFERENCES [dbo].[dna_gen] ([dna_gen_id])
        
        ALTER TABLE [dbo].[dna] CHECK CONSTRAINT [FK_dna_dna_gen_id]
        
        ALTER TABLE [dbo].[dna]  WITH CHECK ADD  CONSTRAINT [FK_dna_pacient] FOREIGN KEY([dna_pacient_id])
        REFERENCES [dbo].[pacient] ([pacient_id])
        
        ALTER TABLE [dbo].[dna] CHECK CONSTRAINT [FK_dna_pacient]
        
        ALTER TABLE [dbo].[dna]  WITH CHECK ADD  CONSTRAINT [FK_dna_typ_vzorku] FOREIGN KEY([dna_typ_vzorku_id])
        REFERENCES [dbo].[typ_vzorku] ([typ_vzorku_id])
        
        ALTER TABLE [dbo].[dna] CHECK CONSTRAINT [FK_dna_typ_vzorku]
        
        ALTER TABLE [dbo].[panel_gen]  WITH CHECK ADD  CONSTRAINT [FK_panel_gen_gen] FOREIGN KEY([panel_gen_panel_id])
        REFERENCES [dbo].[gen] ([gen_id])
        
        ALTER TABLE [dbo].[panel_gen] CHECK CONSTRAINT [FK_panel_gen_gen]
        
        ALTER TABLE [dbo].[panel_gen]  WITH CHECK ADD  CONSTRAINT [FK_panel_gen_panel] FOREIGN KEY([panel_gen_panel_id])
        REFERENCES [dbo].[panel] ([panel_id])
        
        ALTER TABLE [dbo].[panel_gen] CHECK CONSTRAINT [FK_panel_gen_panel]
        
        ALTER TABLE [dbo].[varianta]  WITH CHECK ADD  CONSTRAINT [FK_varianta_gen] FOREIGN KEY([varianta_gen_id])
        REFERENCES [dbo].[gen] ([gen_id])
        
        ALTER TABLE [dbo].[varianta] CHECK CONSTRAINT [FK_varianta_gen]
        
        ALTER TABLE [dbo].[varianta_vysetreni]  WITH CHECK ADD  CONSTRAINT [FK_varianta_vysetreni_dna_gen] FOREIGN KEY([varianta_vysetreni_dna_gen_id])
        REFERENCES [dbo].[dna_gen] ([dna_gen_id])
        
        ALTER TABLE [dbo].[varianta_vysetreni] CHECK CONSTRAINT [FK_varianta_vysetreni_dna_gen]
        
        ALTER TABLE [dbo].[varianta_vysetreni]  WITH CHECK ADD  CONSTRAINT [FK_varianta_vysetreni_varianta] FOREIGN KEY([varianta_vysetreni_varianta_id])
        REFERENCES [dbo].[varianta] ([varianta_id])
        
        ALTER TABLE [dbo].[varianta_vysetreni] CHECK CONSTRAINT [FK_varianta_vysetreni_varianta]
        
        ALTER TABLE [dbo].[varianta_vysetreni]  WITH CHECK ADD  CONSTRAINT [FK_varianta_vysetreni_vysetreni] FOREIGN KEY([varianta_vysetreni_vysetreni_id])
        REFERENCES [dbo].[vysetreni] ([vysetreni_id])
        ON DELETE CASCADE
        
        ALTER TABLE [dbo].[varianta_vysetreni] CHECK CONSTRAINT [FK_varianta_vysetreni_vysetreni]
        
        ALTER TABLE [dbo].[vysetreni]  WITH CHECK ADD  CONSTRAINT [FK_vysetreni_dna] FOREIGN KEY([vysetreni_dna_id])
        REFERENCES [dbo].[dna] ([dna_id])
        
        ALTER TABLE [dbo].[vysetreni] CHECK CONSTRAINT [FK_vysetreni_dna]
        
        ALTER TABLE [dbo].[vysetreni]  WITH CHECK ADD  CONSTRAINT [FK_vysetreni_panel] FOREIGN KEY([vysetreni_panel_id])
        REFERENCES [dbo].[panel] ([panel_id])
        
        ALTER TABLE [dbo].[vysetreni] CHECK CONSTRAINT [FK_vysetreni_panel]
        
        ALTER TABLE [dbo].[vysetreni]  WITH CHECK ADD  CONSTRAINT [FK_vysetreni_vysetrujici] FOREIGN KEY([vysetreni_vysetrujici_id])
        REFERENCES [dbo].[vysetrujici] ([vysetrujici_id])
        
        ALTER TABLE [dbo].[vysetreni] CHECK CONSTRAINT [FK_vysetreni_vysetrujici]
        
        ALTER TABLE [dbo].[vysetrujici]  WITH NOCHECK ADD  CONSTRAINT [FK_vysetrujici_role] FOREIGN KEY([vysetrujici_role])
        REFERENCES [dbo].[role] ([role_id])
        
        ALTER TABLE [dbo].[vysetreni] ADD  CONSTRAINT [DF_vysetreni_vysetreni_datum]  DEFAULT (getdate()) FOR [vysetreni_datum]
        
        ALTER TABLE [dbo].[vysetrujici] CHECK CONSTRAINT [FK_vysetrujici_role]
        
        ALTER DATABASE ' . $databaseName . ' SET  READ_WRITE 
        
        INSERT INTO [dbo].[role]
                   ([role_nazev])
             VALUES
                   (\'admin\'),
                   (\'user\')

        INSERT INTO [dbo].[dna_gen]
                   ([dna_gen_typ])
             VALUES
                   (\'het\'),
                   (\'hom\')
        
        INSERT INTO [dbo].[typ_vzorku]
                    ([typ_vzorku_id], [typ_vzorku_nazev])
             VALUES
                    (1, \'periferní krev K3EDTA\'),
                    (2, \'periferní krev Na heparin\'),
                    (3, \'plodová voda nativní\'),
                    (4, \'produkty koncepce\'),
                    (5, \'kostní dren\'),
                    (6, \'bukální ster\'),
                    (7, \'kultivované amniocyty\'),
                    (8, \'archivovaná DNA\'),
                    (9, \'periferní krev Na citrát\')                    
                    
        INSERT INTO [dbo].[gen]
                    ([gen_nazev], [gen_reference])
             VALUES
                    (\'BRCA1\', \'LRG 292\'),
                    (\'BRCA2\', \'LRG 293\'),
                    (\'ATM\', \'LRG 135\'),
                    (\'APC\', \'LRG 130\'),
                    (\'BARD1\', \'\'),
                    (\'BRIP1\', \'LRG 300\'),
                    (\'CDH1\', \'LRG 301\'),
                    (\'CHEK2\', \'LRG 302\'),
                    (\'EPCAM\', \'LRG 215\'),
                    (\'MLH1\', \'LRG 216\'),
                    (\'MSH2\', \'LRG 218\'),
                    (\'MSH6\', \'LRG 219\'),
                    (\'MUTYH\', \'LRG 220\'),
                    (\'NBN\', \'LRG 158\'),
                    (\'PALB2\', \'LRG 308\'),
                    (\'PMS2\', \'LRG 161\'),
                    (\'PTEN\', \'LRG 311\'),
                    (\'RAD50\', \'\'),
                    (\'RAD51C\', \'LRG 314\'),
                    (\'RAD51D\', \'LRG 516\'),
                    (\'STK11\', \'LRG 319\'),
                    (\'TP53\', \'LRG 321\')     
                    
        INSERT INTO [dbo].[panel]
                    ([panel_nazev], [panel_poznamka])
            VALUES
                    (\'Panel 1\', \'BRCA\'),
                    (\'Panel 2\', \'HEVA\')
                   
        INSERT INTO [dbo].[panel_gen]
                    ([panel_gen_panel_id], [panel_gen_gen_id])
            VALUES
                    (1, 1),
                    (1, 2),
                    (2, 1),
                    (2, 2),
                    (2, 3),
                    (2, 4),
                    (2, 5),
                    (2, 6),
                    (2, 7),
                    (2, 8),
                    (2, 9),
                    (2, 10),
                    (2, 11),
                    (2, 12),
                    (2, 13),
                    (2, 14),
                    (2, 15),
                    (2, 16),
                    (2, 17),
                    (2, 18),
                    (2, 19),
                    (2, 20),
                    (2, 21),
                    (2, 22)                        
        ';

        return $sqlScript;
    }
}
