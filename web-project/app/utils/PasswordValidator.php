<?php
/**
 * Created by PhpStorm.
 * User: Fifal
 * Date: 23.06.2018
 * Time: 21:58
 */

namespace App\Utils;

class PasswordValidator
{
    /**
     * Checks if password is strong enough
     *
     * @param $password
     * @return bool
     */
    public static function isValidPassword($password){
        // If password length is smaller than 4
        if(strlen($password) < 4){
            return false;
        }

        return true;
    }
}