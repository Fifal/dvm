<?php

/**
 * Created by PhpStorm.
 * User: fifal
 * Date: 24.5.18
 * Time: 16:14
 */

namespace App\Utils\Varsome;


class VarsomeAPI
{
    use \Nette\SmartObject;

    private $apiKey;
    private $sourceDatabases;

    public function __construct($apiKey)
    {
        $this->apiKey = $apiKey;
        $this->sourceDatabases = array();
        $this->addSourceDatabase(VarsomeVariantEnum::ALL);
    }

    /**
     * Returns Variant related data
     *
     * @param $variant : - HGVS Protein-level variant - Gene/transcipt name followed by HGVS Protein level notation.
     *                  Examples: BRAF:V600E, NM_001252678:I182T
     *
     *                  - HGVS DNA-level variant - Gene/transcipt name followed by HGVS DNA level notation.
     *                  Example: FTO:c.46-43098T>C
     *
     *                  - rs_id - the dbSNP accession number. String “rs” followed by one or more digits.
     *                  Example: rs113488022
     *
     *                  - 4-part genomic variant specification- chromosome:position:reference_allele:alternate_allele
     *                  or chromosome:position:reference_length:alternate_allele. The separator may be ‘:' or ‘-‘,
     *                  the chromosome number is optionally preceded by the string “chr”,
     *                  and position is the 1-based chromosomal position.
     *
     *                  - variant_id - our 20-digit integer value (example '10190150730273780002’).
     *                  If you call “region_variants” below, it is faster to then obtain variant data using
     *                  the variant_ids returned.
     * @param $refGenome : `hg19` or `hg38` Default: `hg19`
     * @return array of VarsomeVariant
     */
    public function getVariantLookup($variant, $refGenome = null)
    {
        $refGen = $refGenome == null ? 'hg19' : 'hg38';
        // ncbi-clinvar2, jenom pro jistotu aby byl přístup do ClinVar s verdictem, protože jinam nemáme přístup.
        $url = 'https://api.varsome.com/lookup/' . $variant . '/' . $refGen;

        $result = $this->getResult($url, false, null);
        $variants = $this->getVariantResultObject($result);

        return $variants;
    }

    //TODO: Nefunguje jim to, vrací UNEXPECTED ERROR OCCURED

    /**
     * Retrieve variant data for more than one variants which are passed in the POST request payload,
     * based on a refrence genome id. This is currently limited to 1000 variants per request.
     */
    public function getBatchVariantLookup($variantsArray, $refGenome = null)
    {
        $refGen = $refGenome == null ? 'hg19' : 'hg38';
        $url = 'https://api.varsome.com/lookup/batch/' . $refGen;

        $postFields = array('variants' => $variantsArray);
        $postJson = json_encode($postFields);

        $result = $this->getResult($url, true, $postJson);
        return $result;
    }

    /**
     * Returns all known variants inside the genomic region described, using the ref_genome,
     * chromosome_id, position and length.
     *
     * @param $refGenome : `hg19` or `hg38` Default: `hg19`
     * @param $chromosomeId : A number representing the chromosome, 1-22, 23 for X and 24 for Y.(example `1`)
     * @param $position : the 1-based chromosomal position of the start of the region.
     * @param $length : the length of the region in base pairs
     *
     * @return mixed
     */
    public function getRegionVariants($refGenome, $chromosomeId, $position, $length)
    {
        $refGen = $refGenome == null ? 'hg19' : 'hg38';
        $url = 'https://api.varsome.com/region_variants/' . $refGen . '/' . $chromosomeId . '/'
            . $position . '/' . $length;

        return $this->getResult($url, false, null);
    }

    /**
     * Returns gene data for the given 'geneSymbol'.
     *
     * @param $geneSymbol : The gene's symbol
     * @param $refGenome : `hg19` or `hg38` Default: `hg19`
     * @return mixed
     */
    public function getGeneLookup($geneSymbol, $refGenome)
    {
        $refGen = $refGenome == null ? 'hg19' : 'hg38';
        $url = 'https://api.varsome.com/lookup/gene/' . $geneSymbol . '/' . $refGen;

        $result = $this->getResult($url, false, null);
        $gene = $this->getGeneResultObject($result);

        return $gene;
    }

    //TODO: NEFUNGUJE JIM TO!

    /**
     * Retrieve variant data for more than one variants which are passed in the POST request payload,
     * based on a reference genome id. This is currently limited to 1000 variants per request.
     */
    public function getBatchGeneLookup($genomesArray, $refGenome = null)
    {
        $refGen = $refGenome == null ? 'hg19' : 'hg38';
        $url = 'https://api.varsome.com/lookup/genes/batch/' . $refGen;

        $postFields = array('genes' => $genomesArray);
        $postJson = json_encode($postFields);

        $result = $this->getResult($url, true, $postJson);
        return $result;
    }

    /**
     * Returns transcript related data.
     *
     * @param $transcript : The transcript name
     * @param $refGenome : `hg19` or `hg38` Default: `hg19`
     *
     * @return mixed
     */
    public function getTranscriptLookup($transcript, $refGenome = null)
    {
        $refGen = $refGenome == null ? 'hg19' : 'hg38';
        $url = 'https://api.varsome.com/lookup/transcript/' . $transcript . '/' . $refGen;

        return $this->getResult($url, false, null);
    }

    /**
     * Returns curl GET/POST request
     *
     * @param $url : GET url
     * @param $postBool : true for POST, false for GET request
     * $param $postFields: POST fields
     * @return mixed HTTP response header and json with response
     */
    private function getResult($url, $postBool, $postFields)
    {
        ini_set('max_execution_time', 120);

        if (count($this->sourceDatabases) > 0) {
            $url = $url . '?add-source-databases=';
        }

        // přidání zdrojových databází jako parametr url adresy (add-source-databases)
        for ($i = 0; $i < count($this->sourceDatabases); $i++) {
            if ($i == count($this->sourceDatabases) - 1)
                $url = $url . $this->sourceDatabases[$i];
            else
                $url = $url . $this->sourceDatabases[$i] . ',';
        }

        $curl = curl_init();

        // přidání api tokenu do HTTP headeru
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Authorization: Token ' . $this->apiKey
        ));

        // pokud se jedná o request typu POST
        if ($postBool) {
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $url,
                CURLOPT_USERAGENT => 'User Agent X',
                CURLOPT_POSTFIELDS => $postFields
            ));

            var_dump($url);
            var_dump($postFields);
        }
        // request typu GET
        else {
            curl_setopt_array($curl, array(
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => $url,
                CURLOPT_USERAGENT => 'User Agent X'
            ));
        }

        $response = curl_exec($curl);
        curl_close($curl);

        return $response;
    }

    /**
     * Returns the schema of a variant response object, containing relevant information for each field included
     * in the variant lookup response.
     *
     * @return mixed
     */
    public function getVariantSchema()
    {
        $url = 'https://api.varsome.com/lookup/schema';

        return $this->getResult($url, false, null);
    }

    /**
     * Returns the schema of a gene response object, containing relevant information for each field included
     * in the gene lookup response.
     *
     * @return mixed
     */
    public function getGeneSchema()
    {
        $url = 'https://api.varsome.com/lookup/schema/genes';

        return $this->getResult($url, false, null);
    }

    /**
     * Adds new source DB to array
     *
     * @param $dbEnum
     */
    public function addSourceDatabase($dbEnum)
    {
        array_push($this->sourceDatabases, $dbEnum);
    }

    /**
     * @param $responseJson: Response JSON from Varsome API
     * @return array: array of VarsomeVariant
     */
    private function getVariantResultObject($responseJson)
    {
        $parsed = json_decode($responseJson);
        $result = array();

        // v json odpovědi se nachází víc než jeden výsledek -> je to pole
        if (is_array($parsed)) {

            // pro každej výsledek se vytvoří přepravka pro variantu
            foreach ($parsed as $item) {
                $variant = new VarsomeVariant();

                // parsování jednotlivých dat
                if (isset($item->ncbi_clinvar2[0]->main_data)) {
                    $variant->setClinvarVerdict($item->ncbi_clinvar2[0]->main_data);
                }
                if (isset($item->ncbi_clinvar2[0]->last_evaluation)) {
                    $variant->setLastEvalDate($item->ncbi_clinvar2[0]->last_evaluation);
                }
                if (isset($item->ncbi_dbsnp[0]->rsid)) {
                    $variant->setRsid($item->ncbi_dbsnp[0]->rsid);
                }
                if(isset($item->refseq_transcripts[0]->items[0]->hgvs) && isset($item->refseq_transcripts[0]->items[0]->gene_symbol)){
                    $variant->setHgvs($item->refseq_transcripts[0]->items[0]->gene_symbol . ':' . $item->refseq_transcripts[0]->items[0]->hgvs);
                }

                // přidání výsledku do pole
                array_push($result, $variant);
            }
        } else {
            $variant = new VarsomeVariant();

            // parsování jednotlivých dat
            if (isset($parsed->ncbi_clinvar2[0]->main_data)) {
                $variant->setClinvarVerdict($parsed->ncbi_clinvar2[0]->main_data);
            }
            if (isset($parsed->ncbi_clinvar2[0]->last_evaluation)) {
                $variant->setLastEvalDate($parsed->ncbi_clinvar2[0]->last_evaluation);
            }
            if (isset($parsed->ncbi_dbsnp[0]->rsid)) {
                $variant->setRsid($parsed->ncbi_dbsnp[0]->rsid);
            }
            if(isset($parsed->refseq_transcripts[0]->items[0]->hgvs) && isset($parsed->refseq_transcripts[0]->items[0]->gene_symbol)){
                $variant->setHgvs($parsed->refseq_transcripts[0]->items[0]->gene_symbol . ':' .  $parsed->refseq_transcripts[0]->items[0]->hgvs);
            }

            array_push($result, $variant);
        }

        return $result;
    }

    /**
     * @param $responseJson: Response JSON from Varsome API
     * @return array: array of VarsomeGene
     */
    private function getGeneResultObject($responseJson)
    {
        $parsed = json_decode($responseJson);
        $result = array();
        $synonyms = array();

        // Víc než jeden výsledek
        if (is_array($parsed)) {
            $gene = new VarsomeGene();

            // parsování jednotlivých dat
            foreach ($parsed as $item) {
                if (isset($item->symbol)) {
                    $gene->setSymbol($item->symbol);
                }
                if (isset($item->gene_id)) {
                    $gene->setGeneId($item->gene_id);
                }
                if (isset($item->description)) {
                    $gene->setDescription($item->description);
                }
                if(isset($item->synonyms)){
                    foreach ($item->synonyms as $synonym){
                        array_push($synonyms, $synonym);
                    }
                }
                if(isset($item->identifiers->lrg_id)){
                    $gene->setReference($item->identifiers->lrg_id);
                }
                $gene->setSynonyms($synonyms);
                array_push($result, $gene);
            }
        } else {
            $gene = new VarsomeGene();

            // parsování jednotlivých dat
            if (isset($parsed->symbol)) {
                $gene->setSymbol($parsed->symbol);
            }
            if (isset($parsed->gene_id)) {
                $gene->setGeneId($parsed->gene_id);
            }
            if (isset($parsed->description)) {
                $gene->setDescription($parsed->description);
            }
            if(isset($parsed->synonyms)){
                foreach ($parsed->synonyms as $synonym){
                    array_push($synonyms, $synonym);
                }
            }
            if(isset($parsed->identifiers->lrg_id[0])){
                $gene->setReference($parsed->identifiers->lrg_id[0]);
            }
            $gene->setSynonyms($synonyms);
            array_push($result, $gene);
        }

        return $result;
    }
}